from symbol import *
from ast import *
from error import *
from symcheck import SymbolResolver
from typecheck import TypeChecker

class Sema:
    def __init__(self, compiler, module):
        self.compiler = compiler
        self.module = module
        self.symbol_table = SymbolTable(Scope(module.name))


    def resolveSymbols(self):
        symbol_resolver = SymbolResolver(self.compiler, self.module, self.symbol_table)
        symbol_resolver.collectDeclarations()
        if not symbol_resolver.checkReferences():
            print("checkReferences() failed")
            return False
        return True

    def collectAndCheckTypes(self):
        type_checker = TypeChecker(self.compiler, self.module, self.symbol_table)
        if not type_checker.collectTypes():
            return False
        if not type_checker.checkTypes():
            return False
        return True
