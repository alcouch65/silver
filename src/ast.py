
class ASTElement:
    def __init__(self, name, loc):
        self.name = name
        self.loc = loc

    def dump(self):
        pass

class ASTModule(ASTElement):
    def __init__(self, name, loc, path):
        super().__init__(name, loc)
        self.path = path
        self.symbols = []
        self.body = []

    def dump(self):
        dump = f"\\mod {self.name}@\"{self.path}\"\n"
        for stmt in self.body:
            stmt_dump = stmt.dump()
            lines = stmt_dump.splitlines()
            for line in lines:
                dump += f"    {line}\n"
        return dump

class ASTStatement(ASTElement):
    def __init__(self, name, loc):
        super().__init__(name, loc)


class ASTVariable(ASTStatement):
    ##ident = str
    ##typ = ASTType
    ##expr = ASTExpression
    def __init__(self, ident, loc, typ, expr):
        super().__init__("var", loc)
        self.ident = ident
        self.type = typ
        self.expr = expr

    def dump(self):
        return f"\\var %{self.ident} {self.type.dump()} {self.expr.dump()}\n"
        

class ASTType(ASTElement): 
    ##kind = str
    ##name = str
    ##data = dict or None
    def __init__(self, kind, name, loc, data):
        super().__init__(name, loc)
        self.data = data
        self.kind = kind
        self.name = name


    def dump(self):
        if self.kind == "primitive":
            if self.name == "ref":
                return f"\\type ref {self.data['type'].dump()}"
            else:
                return self.name
        elif self.kind == "aggregate":
            if self.name == "array":
                size = self.data['size']
                child = self.data['type']
                return f"\\type [{size}]{child.dump()}"
        else:
            return f"\\type {self.kind}:{self.name}"

class ASTExpression(ASTStatement):
    def __init__(self, name, loc):
        super().__init__(name, loc)
        self.type = None

class ASTVarCopy(ASTExpression):
    ##ident = str
    def __init__(self, loc, ident, arg=False):
        super().__init__("copy", loc)
        self.type = None
        self.ident = ident
        self.arg = arg

    def dump(self):
        return f"\\copy {self.ident}"

class ASTInteger(ASTExpression):
    ##value = int
    def __init__(self, name, loc, value):
        super().__init__(name, loc)
        self.type = ASTType("primitive", name, loc, None)
        self.value = value

    def dump(self):
        return f"\\{self.type.dump()} {self.value}"

class ASTBinary(ASTExpression):
    ##lhs = ASTExpression
    ##rhs = ASTExpression
    ##typ = ASTType
    def __init__(self, name, loc, lhs, rhs, typ):
        super().__init__(name, loc)
        self.type = typ
        self.lhs = lhs
        self.rhs = rhs

    def dump(self):
        dump = f"\\{self.name} "
        if self.type is not None:
            dump += f"{self.type.dump()} "
        dump += f"{self.lhs.dump()} {self.rhs.dump()}"
        return dump

class ASTCharLiteral(ASTExpression):
    ##value = char/u8
    def __init__(self, loc, value):
        super().__init__("char", loc)
        self.value = value

    def dump(self):
        return f"\\char {self.value}"

class ASTRef(ASTExpression):
    ##refee = ASTExpression
    def __init__(self, loc, refee):
        super().__init__("ref", loc)
        self.refee = refee
        self.type = ASTType("primitive", "ref", loc, None) 
        self.arg = False

    def dump(self):
        return f"\\ref {self.refee}"

class ASTFunCall(ASTExpression):
    ##func_name = str
    ##args = []ASTExpression
    def __init__(self, loc, func_name, args):
        super().__init__("call", loc)
        self.func_name = func_name
        self.args = args

    def dump(self):
        dump = f"\\call {self.func_name}"
        if len(self.args) > 0:
            dump += " "
        for index, arg in enumerate(self.args):
            dump += f"\\arg {arg.dump()}"
            if index < len(self.args) - 1:
                dump += " "
        return dump


class ASTArrayLiteral(ASTExpression):
    ##size = int
    ##elem_type = ASTType
    ##elements = []ASTExpression
    def __init__(self, loc, size, elem_type, elements):
        super().__init__("array", loc)
        self.size = size
        self.elem_type = elem_type
        self.elements = elements

    def dump(self):
        dump = f"\\array({self.size})"
        if self.elem_type is not None:
            dump += f" {self.elem_type.dump()}"
        if len(self.elements) > 0:
            dump += " "

        for index, elem in enumerate(self.elements):
            dump += f"\\elem {elem.dump()}"
            if index < len(self.elements):
                dump += " "

        return dump

class ASTFunction(ASTStatement):
    ##arity = int
    ##params = []ASTParam
    ##ret_type = ASTType
    ##body = []ASTStatement
    def __init__(self, loc, ident, arity, params, ret_type, body):
        super().__init__("fun", loc)
        self.ident = ident
        self.arity = arity
        self.params = params
        self.body = body
        self.ret_type = ret_type

    def dump(self):
        dump = f"\\fun {self.ident} \\type {self.ret_type.dump()} \\arity {self.arity} "

        if self.arity > 0:
            for index, param in enumerate(self.params):
                dump += param.dump()
                if index < self.arity - 1:
                    dump += " "

        if len(self.body) > 0:
            dump += " -> \n"
            for stmt in self.body:
                dump += f"    {stmt.dump()}"
        return dump

class ASTArrayIndex(ASTElement):
    def __init__(self, loc, array_ident, index_expr):
        super().__init__("arr_index", loc)
        self.array_ident = array_ident
        self.index_expr = index_expr

    def dump(self):
        return f"\\arr_index {self.array_ident} {self.index_expr.dump()}"

class ASTArraySet(ASTStatement):
    ##ident = str
    ##index = ASTArrayIndex
    ##expr = ASTExpression
    def __init__(self, loc, index, expr):
        super().__init__("set", loc)
        self.index = index
        self.expr = expr

    def dump(self):
        return f"\\set {self.index.dump()} {self.expr.dump()}\n"

class ASTArrayGet(ASTExpression):
    def __init__(self, loc, array_index):
        super().__init__("get", loc)
        self.index = array_index

    def dump(self):
        return f"\\get {self.index.dump()}"

class ASTReturn(ASTStatement):
    def __init__(self, loc, expr):
        super().__init__("return", loc)
        self.expr = expr

    def dump(self):
        return f"\\return {self.expr.dump()}\n"

class ASTFuncParam(ASTElement):
    def __init__(self, loc, ident, typ):
        super().__init__("param", loc)
        self.ident = ident
        self.type = typ

    def dump(self):
        return f"\\param {self.ident} {self.type.dump()}"

