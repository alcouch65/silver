from iota import iota

COLOR_RED       = iota(True)
COLOR_BLUE      = iota()
COLOR_YELLOW    = iota()
COLOR_GREEN     = iota()

def display_debug(loc=None, src_path="", src_line=None, message=""):
    display_log(loc, src_path, src_line, COLOR_GREEN, f"Debug: {message}")

def display_warning(loc=None, src_path="", src_line=None, message=""):
    display_log(loc, src_path, src_line, COLOR_GREEN, f"Warning: {message}")

def display_info(loc=None, src_path="", src_line=None, message=""):
    display_log(loc, src_path, src_line, COLOR_GREEN, f"Info: {message}")

def display_error(loc=None, src_path="", src_line=None, message=""):
    display_log(loc, src_path, src_line, COLOR_RED, f"Error: {message}")

def display_log(loc=None, src_path="", src_line=None, color=None, message=""):
    if color == COLOR_RED:
        color = "\033[0;31m"
    elif color == COLOR_BLUE:
        color = "\033[0;34m"
    elif color == COLOR_YELLOW:
        color = "\033[0;33m"
    elif color == COLOR_GREEN:
        color = "\033[0;32m"
    reset = "\033[0m"
    if loc and src_path and src_line:
        line = loc['line']
        start = loc['start_col']
        end = loc['end_col']
        print(f"[{src_path}:{line+1}:{start + 1}] {color}{message}{reset}")
        before = ""
        if start > 0:
            before = src_line[:start]
        if start == end:
            after = src_line[end+1:]
        else:
            after = src_line[end:]

        offender = ""
        if start > 0:
            if start == end:
                offender = src_line[start]
            else:
                offender = src_line[start:end]
        else:
            offender = src_line[0:end]
        print(f"    {before}{color}{offender}{reset}{after}")
    else:
        print(f"[{src_path}] {color}{message}{reset}")
