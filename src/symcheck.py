from symbol import *
from error import *
from ast import *

class SymbolResolver:
    def __init__(self, compiler, module, symbol_table):
        self.compiler = compiler
        self.symbol_table = symbol_table
        self.module = module

    def collectDeclarationsInFunction(self, func):
        for stmt in func.body:
            if isinstance(stmt, ASTVariable):
                self.symbol_table.push_variable(stmt)
            elif isinstance(stmt, ASTFunction):
                self.collectDeclarationsInFunction(stmt) #TODO: Should this be allowed? Inner functions? Sure?

    def collectDeclarations(self):
        for stmt in self.module.body:
            if isinstance(stmt, ASTFunction):
                self.symbol_table.push_function(stmt)
                self.collectDeclarationsInFunction(stmt)
                self.symbol_table.pop_scope()
            elif isinstance(stmt, ASTVariable):
                self.symbol_table.push_variable(stmt)

    def checkExpression(self, expr):
        if isinstance(expr, ASTVarCopy):
            print(f"Looking for ident {expr.ident}")
            if not self.symbol_table.has_variable(expr.ident):
                if not self.symbol_table.has_param(expr.ident):
                    display_error(
                        expr.loc,
                        self.compiler.src_path,
                        self.compiler.src_str.splitlines()[expr.loc['line']],
                        f"No variable named {expr.ident}"
                    )
                    return False
                ##We need to set this so that codegen knows how to access this variable
                expr.arg = True
        elif isinstance(expr, ASTRef):
            if not self.symbol_table.has_variable(expr.refee):
                if not self.symbol_table.has_param(expr.refee):
                    display_error(
                        expr.loc,
                        self.compiler.src_path,
                        self.compiler.src_str.splitlines()[expr.loc['line']],
                        f"No variable named {expr.refee}"
                    )
                    return False
                expr.arg = True
        elif isinstance(expr, ASTFunCall):
            if not self.symbol_table.has_function(expr.func_name):
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"No function named {expr.func_name}"
                )
                return False
            for arg in expr.args:
                if not self.checkExpression(arg):
                    return False
        elif isinstance(expr, ASTArrayGet):
            if not self.symbol_table.has_variable(expr.index.array_ident):
                return False
            if not self.checkExpression(expr.index.index_expr):
                return False
        elif isinstance(expr, ASTBinary):
            if not self.checkExpression(expr.lhs):
                return False
            if not self.checkExpression(expr.rhs):
                return False

        elif isinstance(expr, ASTInteger):
            return True
        elif isinstance(expr, ASTArrayLiteral):
            for elem in expr.elements:
                if not self.checkExpression(elem):
                    return False
        else:
            display_error(
                expr.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[expr.loc['line']],
                "Unrecognized expression"
            )
            return False
        return True

    def checkStatement(self, stmt):
        if isinstance(stmt, ASTVariable):
            if not self.checkExpression(stmt.expr):
                return False
        elif isinstance(stmt, ASTFunction):
            self.symbol_table.push_function(stmt)
            for st in stmt.body:
                if not self.checkStatement(st):
                    return False
            self.symbol_table.pop_scope()
        elif isinstance(stmt, ASTReturn):
            if not self.checkExpression(stmt.expr):
                return False
        elif isinstance(stmt, ASTArraySet):
            if not self.symbol_table.has_variable(stmt.index.array_ident):
                return False
            if not self.checkExpression(stmt.index.index_expr):
                return False
            if not self.checkExpression(stmt.expr):
                return False
        else:
            display_error(
                stmt.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[stmt.loc['line']],
                "Unrecognized statement"
            )
            return False
        return True

    def checkReferences(self):
        for stmt in self.module.body:
            if not self.checkStatement(stmt):
                return False
        return True
