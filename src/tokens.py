from iota import iota
from string import punctuation

#           Meta
TOKEN_UNKNOWN   = iota()
#           Core Lexemes
TOKEN_WORD      = iota()
TOKEN_INT       = iota()
TOKEN_FLOAT     = iota()
TOKEN_CHAR      = iota()
#           Symbols
TOKEN_EQ        = iota()
TOKEN_PLUS      = iota()
TOKEN_MINUS     = iota()
TOKEN_STAR      = iota()
TOKEN_FSLASH    = iota()
TOKEN_BSLASH    = iota()
TOKEN_LPAREN    = iota()
TOKEN_RPAREN    = iota()
TOKEN_COMMA     = iota()
TOKEN_COLON     = iota()
TOKEN_RANGLE    = iota()
TOKEN_LANGLE    = iota()
TOKEN_LSQUARE   = iota()
TOKEN_RSQUARE   = iota()

puncts = set(punctuation)

default_token_location = {'start_col': 0, 'end_col': 0, 'line': 0}
##This will consolidate two locations while being left associative, biasing the first location
def consolidate_locations(loc1, loc2):
    return {
        'line': loc1['line'],
        'start_col': loc1['start_col'],
        'end_col': loc2['end_col'],
        'indent': loc1['indent']
    }
