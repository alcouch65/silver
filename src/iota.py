
IOTA_COUNT = 0

def iota(reset=False):
    global IOTA_COUNT
    result = IOTA_COUNT
    if reset:
        IOTA_COUNT = 0
        result = IOTA_COUNT
    IOTA_COUNT += 1
    return result

