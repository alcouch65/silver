from asm import *
from ir import *
from error import *

class StackFrame:
    def __init__(self, compiler=None, parent=None, stack_size=None):
        self.stack = [] #This will contain StackElement objects
        self.parent = parent
        self.stack_pointer = 0 #This will reflect the runtime stack pointer
        self.stack_size = stack_size

    def get_element(self, ident):
        for elem in self.stack:
            if elem.ident == ident:
                return elem

        if not self.parent:
            return None
        return self.parent.get_element(ident)
    ##ident = str
    ##size = int
    def push_element(self, ident=None, size=None):
        self.stack_pointer += size
        self.stack.append(StackElement(ident, self.stack_pointer, size))

    def pop_element(self):
        elem = self.stack.pop()
        self.stack_pointer -= elem.size

class StackElement:
    def __init__(self, ident=None, offset=None, size=None):
        self.ident = ident
        self.offset = offset
        self.size = size

class Function:
    def __init__(self, ident, ret_type, params):
        self.ident = ident
        self.ret_type = ret_type
        self.params = params
        self.routine = ASMRoutine(ident)

class IRLowering:
    def __init__(self, compiler=None, module=None, symbol_table=None):
        self.compiler = compiler
        self.module = module
        self.symbol_table = symbol_table
        self.stack_frame = StackFrame()
        self.asm_module = ASMModule()# noqa


    def get_size_of_array(self, array):
        size = array.data['size']
        return size * self.get_size_of_type(array.data['type'])

    def get_size_of_type(self, expr_type):
        if expr_type.name in ["u8", "u16", "u32", "u64"]:
            return self.get_size_of_integer(expr_type)
        elif expr_type.name == "array":
            return self.get_size_of_array(expr_type)
        return 0

    def get_size_of_integer(self, integer):
        size = 0
        if integer.name == "u8":
            size = 1
        elif integer.name == "u16":
            size = 2
        elif integer.name == "u32":
            size = 4
        elif integer.name == "u64":
            size = 8
        return size

    ##TODO: Finish this method
    def lower_param_ref(self, routine, dest, expr):
        pass

    def lower_ref(self, routine, dest, expr):
        ref_ident = expr.refee
        if expr.arg:
            return self.lower_param_ref(routine, dest, expr)
        ref_var = self.symbol_table.get_variable(ref_ident)
        if not ref_var:
            display_error(
                expr.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[expr.loc['line']],
                f"Could not find variable named {ref_ident}"
            )
            return False

        stack_var = self.stack_frame.get_element(ref_ident)
        if not stack_var:
            display_error(
                expr.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[expr.loc['line']],
                f"Could not find variable named {ref_ident}"
            )
            return False

        offset = stack_var.offset
        routine.routine.instructions.append(
            ASMMov(
                dest,
                ##[rbp-offset]
                ASMPointer(
                    #address
                    ASMRegister("rbp"),
                    #base
                    -offset
                ),
                "qword"
            )
        )
        return True

    def lower_binary(self, routine, dest, expr):
        lhs = self.lower_expression_to_register(routine, "eax", expr.lhs)
        if not lhs:
            ##TODO: Add error. Feeling kinda lazy right now
            return False

        rhs = self.lower_expression_to_register(routine, "ebx", expr.rhs)
        if not rhs:
            ##TODO: Add error. Feeling kinda lazy right now
            return False

        ##x86(_64) is kinda weird with arith instructions
        ##So because mul and other variants only take a source operand
        ##  we gotta isolate it in our code :(
        if expr.name == "mul":
            routine.routine.instructions.append(
                ASMMul(
                    ASMRegister("ebx")
                )
            )
        else:
            routine.routine.instructions.append(
                ASMBinary(
                    expr.name, 
                    ASMRegister("eax"),
                    ASMRegister("ebx")
                )
            )
        ##Here, we don't want to have erroneous moves to the same location
        ##Because mov eax, eax is dumb and a waste of bytes
        if isinstance(dest, ASMRegister):
            if dest.name != "eax":
                routine.routine.instructions.append(
                    ASMMov(
                        dest,
                        ASMRegister("eax"),
                        "dword"
                    )
                )
        else:
            ##This is here because we need to be able to move to the stack
            routine.routine.instructions.append(
                ASMMov(
                    dest,
                    ASMRegister("eax"),
                    "dword"
                )
            )
        return True

    def lower_array_get(self, routine, dest, expr):
        array_index = expr.index
        index_ptr = self.lower_array_index(routine, "eax", array_index)
        if not index_ptr:
            return False
        routine.routine.instructions.append(
            ASMMov(
                dest,
                index_ptr,
                "dword"
            )
        )
        return True

    ##TODO: We need to use the passed in routine to get the offset of the passed in param
    ##  The routine param will keep track of everything about the current routine and what to expect
    ##  so that we can get the pointer to the start of the arguments then index into it like a tuple
    ##
    ##  The start of the args will be saved to the stack and we can get that and we can use relative
    ##      addressing to get a specific offset
    ##  And since the args are put on in reverse order we have to go through the params and 
    ##  calculate the offset by adding the sizes of each param we pass over until the index of the
    ##  given param
    def get_param_offset(self, routine, param):
        param_index = self.symbol_table.index_of_param(param.ident)
        offset = 0
        idx = len(routine.params) - 1
        while idx > param_index:
            p = routine.params[idx]
            offset += self.get_size_of_type(p.type)
        return offset

    def lower_param_copy(self, routine, dest, param):
        source_ptr_offset = self.stack_frame.get_element(f"args_addr_{routine.ident}").offset
        offset = self.get_param_offset(routine, param)
        ##Here we are loading the args pointer back into rsi
        ##Note: We don't know if rsi had been used by something else thus the reason we saved it
        ##      to the stack at the beginning of the function call
        rsi = ASMRegister("rsi")
        source_ptr = ASMPointer(
            ASMRegister("rbp"),
            ASMImmediate(-source_ptr_offset)
        )
        routine.routine.instructions.append(
            ASMMov(
                rsi,
                source_ptr,
                "qword"
            )
        )
        ##Now we are using the offset we got for the param and we are loading it
        ##Into the destination we got from dest variable above
        source = ASMPointer(
            rsi,
            ASMImmediate(offset)
        )
        param_size = self.get_size(param.type)
        size = "dword"
        if param_size == 1:
            size = "byte"
        elif param_size == 2:
            size = "word"
        elif param_size == 8:
            size = "qword"
        routine.routine.instructions.append(
            ASMMov(
                dest,
                source,
                size
            )
        )
        return True
    
    def lower_var_copy(self, routine, dest, expr):
        if expr.arg:
            return self.lower_param_copy(routine, dest, expr)
        ident = expr.ident
        var = self.stack_frame.get_element(ident)
        if not var:
            display_error(
                expr.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[expr.loc['line']],
                f"Could not find variable in stack frame {ident}"
            )
            return False
        size = "dword"
        var_size = self.get_size_of_type(expr.type)
        if var_size == 1:
            size = "byte"
        elif var_size == 2:
            size = "word"
        elif var_size == 8:
            size = "qword"
        routine.routine.instructions.append(
            ASMMov(
                dest,
                ASMPointer(
                    ASMRegister("rbp"),
                    ASMImmediate(-var.offset)
                ),
                size
            )
        )
        return True
    def lower_expression(self, routine, dest, expr):
        if isinstance(expr, IRInteger):
            return self.lower_integer_literal(routine, dest, expr)
        elif isinstance(expr, IRArrayLiteral):
            ##FIXME: this will never be called from a lower_expression_to_register call
            ##  because arrays are not compatible with register lowering
            return self.lower_array_literal(routine, expr)
        elif isinstance(expr, IRBinary):
            return self.lower_binary(routine, dest, expr)
        elif isinstance(expr, IRRef):
            return self.lower_ref(routine, dest, expr)
        elif isinstance(expr, IRArrayGet):
            return self.lower_array_get(routine, dest, expr)
        elif isinstance(expr, IRVarCopy):
            return self.lower_var_copy(routine, dest, expr)
        else:
            display_error(
                expr.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[expr.loc['line']],
                f"Unrecognized expression: {expr.dump()}"
            )
            return False
        return True

    def lower_expression_to_stack(self, routine, offset, expr):
        dest = ASMPointer(
            ASMRegister("rbp"),
            ASMImmediate(offset)
        )
        return self.lower_expression(routine, dest, expr)

    def lower_expression_to_register(self, routine, reg, expr):
        return self.lower_expression(routine, ASMRegister(reg), expr)

    def lower_variable(self, routine,var):
        ident = var.ident
        expr = var.expr

        size = self.get_size_of_type(var.type)
        self.stack_frame.push_element(ident, size)
        offset = self.stack_frame.stack_pointer
        if isinstance(expr, IRFunCall):
            if not self.lower_call(routine, ident, expr):
                return False
        elif not self.lower_expression_to_stack(routine, -offset, expr):
            return False
        return True

    def get_size_of_stack_frame(self,  func):
        body = func.body
        frame_size = 0

        ##We are accounting here for the input and output pointers at the start of the frame
        frame_size += 16
        for stmt in body:
            if isinstance(stmt, IRVariable):
                frame_size += self.get_size_of_type(stmt.type)
        frame_size += 16 #This is for saving the return address and the args address
        return frame_size

    def generate_frame_enter(self, routine, func):
        stack_size = self.get_size_of_stack_frame(func)
        self.stack_frame = StackFrame(self.compiler, self.stack_frame, stack_size)
        if not self.symbol_table.push_function(func):
            return False
        routine.routine.instructions.append(
                ASMPush(
                    ASMRegister(
                        "rbp"
                    )
                )
            )
        routine.routine.instructions.append(
                ASMMov(
                    ASMRegister("rbp"),
                    ASMRegister("rsp"),
                    "qword"
                )
            )
        routine.routine.instructions.append(
                ASMBinary(
                    "sub", 
                    ASMRegister("rsp"), 
                    ASMImmediate(stack_size)
                )
            )
        return True

    ##This method will save the address of the arguments to the given function
    ##The start of the arguments will be on the stack and the pointer to them will be
    ##  put into the rsi register
    def save_args_address(self, routine, func):
        self.stack_frame.push_element(f"args_addr_{func.ident}", 8)
        offset = -self.stack_frame.stack_pointer
        routine.routine.instructions.append(
            ASMMov(
                ASMPointer(
                    ASMRegister("rbp"),
                    ASMImmediate(offset)
                ),
                ASMRegister("rsi"),
                "qword"
            )
        )
        return True

    ##This method will save the address of the return value which is given by the caller
    ##The address is a stack address before the new functions stack frame and will be stored
    ##  in the rdi register and will be put back onto the stack inside the new stack frame
    def save_return_address(self, routine, func):
        self.stack_frame.push_element(f"ret_addr_{func.ident}", 8)
        stack_addr_for_ret_addr = self.stack_frame.stack_pointer
        routine.routine.instructions.append(
            ASMMov(
                ASMPointer(
                    ASMRegister("rbp"),
                    ASMImmediate(-stack_addr_for_ret_addr)
                ),
                ASMRegister("rdi")
            )
        )
        return True

    ##This method will generate instructions for leaving the stack
    ##This involves resetting the stack frame to the previous stack pointer
    def generate_frame_leave(self, routine, func):
        self.stack_frame.stack_size
        ##Drop the size of the allocated frame size so that rsp now points at the saved rbp
        ##that was pushed at the frame enter
        routine.routine.instructions.append(
            ASMBinary(
                "add",
                ASMRegister("rsp"),
                ASMImmediate(self.stack_frame.stack_size)
            )
        )
        ##The only thing left on the current stack is now rbp which was pushed at the frame enter
        routine.routine.instructions.append(
            ASMPop(
                ASMRegister("rbp")
            )
        )
        routine.routine.instructions.append(ASMReturn())
        self.stack_frame = self.stack_frame.parent
        return True

    def lower_function(self, func):
        ident = func.ident
        routine = Function(ident, func.ret_type, func.params)

        if not self.generate_frame_enter(routine,func):
            return False
        if not self.save_return_address(routine, func):
            return False
        if not self.save_args_address(routine, func):
            return False
        body = func.body
        for stmt in body:
            if not self.lower_statement(routine, stmt):
                return False
        if not self.generate_frame_leave(routine, func):
            return False
        self.asm_module.routines.append(routine.routine)
        return True

    def lower_integer_literal(self, routine, dest, integer):
        size = self.get_size_of_integer(integer)
        mov_size = "dword"
        if size == 1:
            mov_size = "byte"
        elif size == 2:
            mov_size = "word"
        elif size == 8:
            mov_size = "qword"
        routine.routine.instructions.append(
            ASMMov(
                dest,
                ASMImmediate(
                    integer.value, size
                ),
                mov_size
            )
        )
        return True

    ##This will go through all the elements and lower them
    ##to asm instructions by going through the elements in reverse order
    ##This is because the stack grows downwards on x86 based machines
    ##  so that means when we allocate an array on the stack, we need the
    ##  indexing to be positively incrementing due to the way that asm addressing
    ##  works in nasm and other assemblers
    ##Addressing works by using the following format:
    ##      base + (index*scale) + displacement
    ##  and because the stack grows downward, the assembler isn't going to tolerate something like
    ##      lea rax, [rbp + 16]
    ##  Because that would be addressing beyond the current stack frame, which could be dangerous
    ##  So in order to do array indexing properly, we need to be able to index relative to rbp
    ##  If we have an array as such:
    ##      arr: [16]u32 = ...
    ##      arr[12] = 5
    ##  Then we will want to generate this in a way such that index 12 is calculate backwards
    ##      array_size = 16 * sizeof(u32)
    ##      array_size = 16 * 4
    ##      array_size = 64
    ##  So the array will be allocated 64 bytes onto the stack
    ##      index = 64 - 12 * sizeof(u32)
    ##      index = 64 - 12 * 4
    ##      index = 64 - 48
    ##      index = 16
    ##  Therefore, the generated code will use the stack address rbp - 16 to get the index 12
    ##
    ##      lea rax, [rbp-12]
    ##      mov [rax], 5
    def lower_array_literal(self, routine, array):
        i = array.size - 1
        elem_size = self.get_size_of_type(array.elem_type)
        array_size = array.size * elem_size
        ##TODO: Each element needs to be offsetted with endianness in mind because the cpu is going
        ##      to write upwards while the stack grows downwards
        ##      So when we start at offset 0, then it will write at offset 0 and offset -1 instead of offset 1 and 0.
        offset_start = self.stack_frame.stack_pointer - array_size
        offset_start += elem_size
        while i >= 0:
            ##TODO: We need to fix the fact that all the elements are not homogeonized (is that how you spell it?) in their types. So when we get the size of them, they are their originally inferred type except for the first element
            ##So that means the int elements are only type int except for the first element which is u32
            elem = array.elements[i]
            temp_ident = f"array_elem{i}"
            offset = offset_start + self.get_size_of_type(elem.type) * i
            if not self.lower_expression_to_stack(routine, -offset, elem):
                return False
            i -= 1
        return True

    def generate_save_return_value_address(self, routine, save_ident, call):
        ##The function being called
        func = self.symbol_table.get_function(call.func_name)
        ##The return type of the function
        return_type = func.ir_element.ret_type
        ##The size of the return type to allocate on the stack
        return_size = self.get_size_of_type(return_type)
        ##Push an element to represent the return value address on the stack
        self.stack_frame.push_element(save_ident, return_size)
        ##The offset on the current stack frame where the return value will be put to
        offset = self.stack_frame.stack_pointer
        ##Append the LEA instruction for loading this stack address to rdi
        routine.routine.instructions.append(
            ASMLEA(
                ASMRegister("rdi"),
                ASMPointer(
                    ASMRegister("rbp"),
                    ASMImmediate(-offset)
                )
            )
        )
        

        return True

    def generate_save_args_value_address(self, routine, call):
        routine.routine.instructions.append(
            ASMLEA(
                ASMRegister("rsi"),
                ASMPointer(
                    ASMRegister("rbp"),
                    ASMImmediate(-self.stack_frame.stack_pointer)
                )
            )
        )
        return True

    def lower_call_arguments(self, routine, call):
        func = self.symbol_table.get_function(call.func_name)
        if not func:
            display_error(
                call.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[call.loc['line']],
                f"No function named {call.func_name}"
            )
            return False
        args = call.args
        arg_idx = len(args) - 1
        while arg_idx >= 0:
            ##The argument we are lowering
            arg = args[arg_idx]
            ##The param this arg is being passed to
            param = func.ir_element.params[arg_idx]
            ##The identifier of the parameter
            ident = param.ident
            ##The offset of the current argument
            offset = self.stack_frame.stack_pointer
            ##The size of the argument
            arg_size = self.get_size_of_type(arg.type)
            ##Push the argument to the stack so the function has access to it via the parameter
            self.stack_frame.push_element(ident, arg_size)
            ##Lower the argument expression to stack using the parameter ident
            expr_result = self.lower_expression_to_stack(routine, -offset, arg)
            if not expr_result:
                return False
            ##Decrement the argument because we are going backwards cause that's how x86 rolls
            arg_idx -= 1
        return True

    ##Cleanup the arguments that were put onto the stack so we don't accidentally generate
    ##  assembly code that regards the arguments that were only used by the called function
    def cleanup_args(self, call):
        arg_idx = len(call.args) - 1
        while arg_idx >= 0:
            ##We want to pop the current element off the stack
            self.stack_frame.pop_element()
            arg_idx -= 1
        return True

    ##We will lower each expression to the stack before calling the function
    ##The way that these expressions are used are by the sizes relative to the start of the stack frame
    ##We will need to push them on in reverse order to the stack and the function should know how
    ##to get them based on the sizes of the parameters
    ##
    ##  def calc(a: int, b: int, c: int) -> int:
    ##      return a + b * c
    ##
    ##  result = calc(5, 3, 8)
    ##The function `calc` should know where a is since a is of type int, which is always turned into u32,
    ##  then `calc` knows that `a` will be rbp - 3 * sizeof(u32) -> rbp - 3 * 4 -> rbp - 12
    ##
    ##Also since that calling `calc` results in the return value being stored in a register,
    ##  the codegen will generate an instruction to move that value to the stack
    def lower_call(self, routine, save_ident, call):
        if not self.generate_save_return_value_address(routine, save_ident, call):
            return False
        if not self.generate_save_args_value_address(routine, call):
            return False
        if not self.lower_call_arguments(routine, call):
            return False
        routine.routine.instructions.append(
            ASMCall(
                call.func_name
            )
        )
        if not self.cleanup_args(call):
            return False
        return True

    def lower_array_index(self, routine, dest, arr_index):
        ##This is the array we are setting at index to
        arr_var = self.symbol_table.get_variable(arr_index.array_ident)
        arr_var_type = arr_var.ir_element.type
        arr_elem_size = self.get_size_of_type(arr_var_type.data['type'])
        ##This is the current stack frame information for this variable so we can get the offset of it
        ##FIXME: Maybe consolidate the upper two lines somehow?
        arr_var_stack_elem = self.stack_frame.get_element(arr_index.array_ident)
        arr_var_offset = arr_var_stack_elem.offset
        arr_var_end_offset = arr_var_offset + arr_elem_size * arr_var_type.data['size']
        
        indexed_pointer = ASMPointer(
            ASMRegister("rbp"),
            ASMRegister("rax"),
            ASMImmediate(arr_elem_size),
            ASMImmediate(-arr_var_end_offset)

        )
        expr = arr_index.index_expr
        if not self.lower_expression_to_register(routine, dest, expr):
            return None
        return indexed_pointer

    def lower_array_set(self, routine, arr_set):
        index = arr_set.index
        expr = arr_set.expr
        dest_reg = "eax"
        source_reg = "ebx"
        if not self.lower_expression_to_register(routine, source_reg, expr):
            return False
        index_ptr = self.lower_array_index(routine, dest_reg, index)
        if not index_ptr:
            return False
        routine.routine.instructions.append(
            ASMMov(
                index_ptr,
                ASMRegister("ebx"),
                "dword"
            )
        )
        return True

    def lower_statement(self, routine, statement):
        if isinstance(statement, IRVariable): # noqa
            if not self.lower_variable(routine, statement):
                return False
        elif isinstance(statement, IRFunction):# noqa
            if not self.lower_function(statement):
                return False
        elif isinstance(statement, IRArraySet):# noqa
            if not self.lower_array_set(routine, statement):
                return False
        elif isinstance(statement, IRReturn):# noqa
            if not self.lower_expression(routine, ASMPointer(ASMRegister("rdi")), statement.expr):
                return False
            #routine.routine.instructions.append(ASMReturn())# noqa
        return True

    def generate_program_exit(self, routine):
        routine.routine.instructions.append(
            ASMMov(
                ASMRegister("rax"),
                ASMImmediate(60, 8),
                "qword"
            )
        )
        routine.routine.instructions.append(
                ASMMov(
                    ASMRegister("rdi"),
                    ASMImmediate(0, 8),
                    "qword"
                )
            )
        routine.routine.instructions.append(
                ASMSyscall()
            )
        return True

    def generate_main_frame_enter(self, routine):
        routine.routine.instructions.append(
                ASMPush(ASMRegister("rbp"))
        )
        routine.routine.instructions.append(
                ASMMov(ASMRegister("rbp"), ASMRegister("rsp"), "qword"))
        routine.routine.instructions.append(
                ASMBinary("sub", ASMRegister("rsp"), ASMImmediate(16, 8)))
        return True

    def lower(self):
        routine = Function("_start", IRType("primtive", "empty", {'line': 0, 'start_col': 0, 'end_col': 0}, None), [])
        ##TODO: The start code needs to be wrapped in a _start function and processed like all the other
        if not self.generate_main_frame_enter(routine):
            return False
        ##  functions
        stack_size = self.get_size_of_stack_frame(self.module)
        self.stack_frame = StackFrame(self.compiler, None, stack_size)
        for statement in self.module.body:
            if not self.lower_statement(routine, statement):
                return False
        if not self.generate_program_exit(routine):
            return False
        self.asm_module.routines.append(routine.routine)
        return True
