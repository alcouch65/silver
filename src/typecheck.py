from ast import *
from type_builder import *
from error import *

class TypeChecker:
    def __init__(self, compiler=None, module=None, symbol_table=None):
        self.compiler = compiler
        self.module = module
        self.symbol_table = symbol_table
        self.type_builder = TypeBuilder(compiler)


    ##
    ##      TYPE COLLECTION METHODS
    ##

    def collectTypeForExpression(self, expr):
        if isinstance(expr, ASTVarCopy):
            var_ident = expr.ident
            var = None
            if self.symbol_table.has_param(var_ident):
                var = self.symbol_table.get_param(var_ident)
            elif self.symbol_table.has_variable(var_ident):
                var = self.symbol_table.get_variable(var_ident)
            else:
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"Cannot acquire type of non-existent variable {var_ident}"
                )
                return False
            expr.type = var.ast_element.type
            return True
        elif isinstance(expr, ASTBinary):
            if not self.collectTypeForExpression(expr.lhs):
                return False
            expr.type = expr.lhs.type
            return True
        elif isinstance(expr, ASTInteger):
            expr.type = self.type_builder.create_int(expr.loc)
            return True
        elif isinstance(expr, ASTRef):
            ##TODO: Add other means of referencing. Right now only variables can be referenced
            #if not self.collectTypeForExpression(expr.refee):
            #    return False
            if self.symbol_table.has_param(expr.refee):
                var = self.symbol_table.get_param(expr.refee)
            elif self.symbol_table.has_variable(expr.refee):
                var = self.symbol_table.get_variable(expr.refee)
            else:
                return False
                        
            expr.type.data = {'type': var.ast_element.type}
            return True
        elif isinstance(expr, ASTArrayLiteral):
            first_elem = expr.elements[0]
            if not self.collectTypeForExpression(first_elem):
                return False
            expr.elem_type = first_elem.type
            expr.type = self.type_builder.create_array(expr.size, expr.elem_type, expr.loc)
            return True
        elif isinstance(expr, ASTArrayGet):
            arr_index = expr.index
            if not self.collectTypeForExpression(arr_index.index_expr):
                return False
            if not self.symbol_table.has_variable(arr_index.array_ident):
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"No variable named {arr_index.array_ident}"
                )
                return False
            var = self.symbol_table.get_variable(arr_index.array_ident).ast_element
            if var.type.name != "array":
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"Expected variable to be of type array..."
                )
                
                display_error(
                    var.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[var.loc['line']],
                    f"...but instead found variable of type {var.type.dump()}"
                )
            expr.type = var.type.data['type']
            return True
        elif isinstance(expr, ASTFunCall):
            fun_ident = expr.func_name
            if not self.symbol_table.has_function(fun_ident):
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"Cannot find function with name {fun_ident}"
                )
                return False
            for arg in expr.args:
                if not self.collectTypeForExpression(arg):
                    return False
            func = self.symbol_table.get_function(fun_ident).ast_element
            expr.type = func.ret_type
            return True

        else:
            display_error(
                expr.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[expr.loc['line']],
                f"Unrecognized expression"
            )
            return False

    def collectTypeForVariable(self, var):
        if not self.collectTypeForExpression(var.expr):
            return False
        if var.type.name == "infer":
            var.type = var.expr.type
        return True

    def collectTypeForStatement(self, stmt):
        if isinstance(stmt, ASTVariable):
            return self.collectTypeForVariable(stmt)
        elif isinstance(stmt, ASTFunction):
            self.symbol_table.push_function(stmt)
            for st in stmt.body:
                if not self.collectTypeForStatement(st):
                    return False
            self.symbol_table.pop_scope()
        elif isinstance(stmt, ASTArraySet):
            array_index = stmt.index
            expr = stmt.expr
            if not self.symbol_table.has_variable(array_index.array_ident):
                return False
            if not self.collectTypeForExpression(array_index.index_expr):
                return False
            if not self.collectTypeForExpression(expr):
                return False
        elif isinstance(stmt, ASTReturn):
            if not self.collectTypeForExpression(stmt.expr):
                return False
        return True


    ##This function will go through the module and fill in any unknown types
    ##So usually types are either filled in during parsing if known, such as literals 
    ##  or if a variable has a type annotation or for function parameters 
    ##  which are required to have a type annotation
    ##  but most of the time are just set to either "infer" or None
    ##  So we need to correct those first before we do any checking/assertions
    def collectTypes(self):
        for stmt in self.module.body:
            if not self.collectTypeForStatement(stmt):
                return False
        return True

    ##
    ##      TYPE CHECKING METHODS
    ##

    def checkStatement(self, stmt):
        if isinstance(stmt, ASTVariable):
            if not self.checkVariable(stmt):
                return False
        elif isinstance(stmt, ASTFunction):
            self.symbol_table.push_function(stmt)
            if stmt.ret_type.name == "int":
                stmt.ret_type.name = "u32"
            for param in stmt.params:
                if param.type.name == "int":
                    param.type.name = "u32"
                elif not self.checkVariable(param):
                    return False
            for st in stmt.body:
                if not self.checkStatement(st):
                    return False
            self.symbol_table.pop_scope()
        elif isinstance(stmt, ASTArraySet):
            array_index = stmt.index
            if not self.checkExpression(array_index.index_expr):
                return False
            if not self.checkExpression(stmt.expr):
                return False
        elif isinstance(stmt, ASTReturn):
            ##Let's compare the types of the current return type and the type of the returned expression
            ##TODO: The parser needs to allow return statements to not have an expression
            ##          then this way we can return nothing
            if not self.expectExpressionType(self.symbol_table.scope.return_type, stmt.expr):
                return False
        return True

    def compareArrays(self, first, second):
        first_data = first.data
        first_child = first_data['type']
        first_size = first_data['size']

        second_data = second.data
        second_child = first_data['type']
        second_size = first_data['size']

        if not self.compareTypes(first_child, second_child):
            return False
        if first_size != second_size:
            display_error(
                first.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[first.loc['line']],
                f"Expected array of size {first_size}..."
            )
            display_error(
                second.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[second.loc['line']],
                f"...but instead found array of size {second_size}"
            )
            return False
        return True

    def compareAggregates(self, first, second):
        if first.name == "array":
            return self.compareArrays(first, second)
        display_error(
            first.loc,
            self.compiler.src_path,
            self.compiler.src_str.splitlines()[first.loc['line']],
            f"Unrecognized aggregate type {first.name}..."
        )
        display_error(
            second.loc,
            self.compiler.src_path,
            self.compiler.src_str.splitlines()[first.loc['line']],
            f"...while comparing against type {second.name}"
        )
        return False

    def compareTypes(self, first, second):
        if first.kind != second.kind:
            return False

        if first.kind == "primitive":
            int_list = ["u8", "u16", "u32", "u64"]
            if first.name == "int":
                if second.name in int_list:
                    return True
                elif second.name == "int":
                    return True
            elif first.name in int_list:
                if second.name == "int":
                    return True
                elif second.name in int_list:
                    if first.name == second.name:
                        return True
                    else:
                        return False
            else:
                return first.name == second.name
        elif first.kind == "aggregate":
            return self.compareAggregates(first, second)
        else:
            display_error(
                second.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[first.loc['line']],
                f"Unrecognized type kind {first.kind}"
            )
        return True

    def checkExpression(self, expr):
        if isinstance(expr, ASTVarCopy):
            var = None
            if self.symbol_table.has_param(expr.ident):
                var = self.symbol_table.get_param(expr.ident)
            elif self.symbol_table.has_variable(expr.ident):
                var = self.symbol_table.get_variable(expr.ident)
            else:
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"Cannot find variable {expr.ident}"
                )
                return False
            return var.ast_element.type
        elif isinstance(expr, ASTFunCall):
            if not self.symbol_table.has_function(expr.func_name):
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"Cannot find function {expr.func_name}"
                )
                return False
            func = self.symbol_table.get_function(expr.func_name).ast_element
            for i in range(func.arity):
                arg = expr.args[i]
                param = func.params[i]
                if not self.compareTypes(arg.type, param.type):
                    return False
            return func.ret_type
        elif isinstance(expr, ASTArrayLiteral):
            elem_type = expr.elem_type
            for elem in expr.elements:
                if not self.compareTypes(elem_type, elem.type):
                    display_error(
                        expr.loc,
                        self.compiler.src_path,
                        self.compiler.src_str.splitlines()[expr.loc['line']],
                        f"Expected array element of type {elem_type.dump()}..."
                    )
                    
                    display_error(
                        expr.loc,
                        self.compiler.src_path,
                        self.compiler.src_str.splitlines()[expr.loc['line']],
                        f"...but instead found element of type {elem.type.dump()}"
                    )
                    return False
                elem.type = elem_type
            return expr.type
        elif isinstance(expr, ASTArrayGet):
            if not self.symbol_table.has_variable(expr.index.array_ident):
                return False
            arr = self.symbol_table.get_variable(expr.index.array_ident).ast_element
            arr_type = arr.type
            if arr_type.name != "array":
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"Expected variable of type array..."
                )
                display_error(
                    arr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[arr.loc['line']],
                    f"...but instead found variable of type {arr_type.dump()}"
                )
                return False
            expr.type = arr_type.data['type']
            return arr_type.data['type']
        elif isinstance(expr, ASTCharLiteral):
            return expr.type
        elif isinstance(expr, ASTInteger):
            if expr.type.name == "int":
                expr.type.name = "u32"
            return expr.type
        elif isinstance(expr, ASTRef):
            return expr.type
        elif isinstance(expr, ASTBinary):
            lhs = self.checkExpression(expr.lhs)
            if not lhs:
                return False
            expr.lhs.type = lhs
            rhs = self.checkExpression(expr.rhs)
            if not rhs:
                return False
            expr.rhs.type = rhs

            rhs = self.checkExpression(expr.rhs)
            expected_type = self.type_builder.create_int(expr.loc)
            if not self.compareTypes(expected_type, expr.lhs.type):
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"Expected type 'int' or variant in left-hand side of binary expression..."
                )
                display_error(
                    expr.lhs.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.lhs.loc['line']],
                    f"...but instead found expression of type {expr.lhs.type.dump()}"
                )
                return False
            
            if not self.compareTypes(expected_type, expr.rhs.type):
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"Expected type 'int' or variant in right-hand side of binary expression..."
                )
                display_error(
                    expr.rhs.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.rhs.loc['line']],
                    f"...but instead found expression of type {expr.rhs.type.dump()}"
                )
                return False
            return expr.type

        return None

    def expectExpressionType(self, ty, expr):
        expr_result = self.checkExpression(expr)
        if not expr_result:
            return False

        if not self.compareTypes(ty, expr_result):
            return False

        return True

    def checkVariable(self, var):
        ty = var.type
        if ty.name == "infer":
            expr_result = self.checkExpression(expr)
            if not expr_result:
                return False
            var.type = expr_result.type
        else:
            if not self.expectExpressionType(ty, var.expr):
                display_error(
                    ty.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[ty.loc['line']],
                    f"Expected type {ty.dump()}..."
                )
                display_error(
                    expr.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[expr.loc['line']],
                    f"...but instead got type {expr.type.dump()}"
                )
                return False
        return True


    def checkTypes(self):
        for stmt in self.module.body:
            if not self.checkStatement(stmt):
                return False
        return True

