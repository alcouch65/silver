from ast import *


class TypeBuilder:
    def __init__(self, compiler=None):
        self.compiler = compiler

    def create_type(self, kind, ident, data, loc):
        return ASTType(kind, ident, loc, data)

    def create_empty(self, loc):
        return self.create_type("primitive", "empty", None, loc)

    def create_infer(self, loc):
        return self.create_type("infer", "infer", None, loc)

    def create_primitive(self, ident, loc):
        return self.create_type("primitive", ident, None, loc)

    def create_int(self, loc):
        return self.create_primitive('u32', loc)

    def create_u8(self, loc):
        return self.create_primitive('u8', loc)

    def create_u16(self, loc):
        return self.create_primitive('u16', loc)

    def create_u32(self, loc):
        return self.create_primitive('u32', loc)

    def create_u64(self, loc):
        return self.create_primitive('u64', loc)

    def create_complex_primitive(self, ident, data, loc):
        return self.create_type("primitive", ident, data, loc)
        
    def create_ref(self, child, loc):
        return self.create_complex_primitive('ref', child, loc)

    def create_aggregate(self, ident, data, loc):
        return self.create_type('aggregate', ident, data, loc)

    def create_tuple(self, types, size, loc):
        return self.create_aggregate(
                "tuple", 
                {'types': types, 'size': size}, 
                loc
            )

    def create_array(self, size, child, loc):
        return self.create_aggregate(
                'array', 
                {'type': child, 'size': size}, 
                loc
            )
