class Symbol:
    def __init__(self, ident, loc, kind, ast_element):
        self.ident = ident
        self.loc = loc
        self.kind = kind
        ##This is a reference to the IR element that this symbol is attached to
        ##This is good for doing cross-checking of types and symbols and semantic evaluation
        self.ast_element = ast_element

class Scope:
    def __init__(self, name, parent=None, return_type = None):
        self.name = name
        self.parent = parent
        self.symbols = []
        self.children = []
        self.return_type = return_type


    def query(self, name, kind):
        for symbol in self.symbols:
            print(f"Comparing symbols: {name} and {symbol.ident}")
            if symbol.ident == name:
                if symbol.kind == kind:
                    return True

        if self.parent is None:
            return False
        return self.parent.query(name, kind)

    def get(self, name, kind):
        for symbol in self.symbols:
            if symbol.ident == name and symbol.kind == kind:
                return symbol

        if self.parent is None:
            return None
        return self.parent.get(name, kind)

    def put(self, ast_elem, kind):
        if self.query(ast_elem.ident, kind):
            return
        self.symbols.append(Symbol(ast_elem.ident, ast_elem.loc, kind, ast_elem))
        
    def index_of(self, name, kind):
        if not self.query(name, kind):
            return None
        for index, symbol in enumerate(self.symbols):
            if symbol.ident == name:
                return index

        if self.parent:
            return self.parent.index_of(name, kind)
        return None


    def index_of_param(self, name):
        return self.index_of(name, "param")

    def get_param(self, name):
        return self.get(name, "param")

    def put_param(self, ast_elem):
        return self.put(ast_elem, "param")

    def has_param(self, name):
        return self.query(name, "param")

    def index_of_variable(self, name):
        return self.index_of(name, "var")

    def get_variable(self, name):
        return self.get(name, "var")

    def put_variable(self, ast_elem):
        self.put(ast_elem, "var")

    def has_variable(self, name):
        return self.query(name, "var")

    def index_of_function(self, name):
        return self.index_of(name, "func")

    def get_function(self, name):
        return self.get(name, "func")

    def put_function(self, ast_elem):
        self.put(ast_elem, "func")
    
    def has_function(self, name):
        return self.query(name, "func")

class SymbolTable:
    def __init__(self, scope):
        self.scope = scope

    def index_of_param(self, name):
        return self.scope.index_of_param(name)

    def has_param(self, name):
        return self.scope.has_param(name)

    def push_param(self, ast_elem):
        self.scope.put_param(ast_elem)

    def get_param(self, name):
        return self.scope.get_param(name)

    def index_of_variable(self, name):
        return self.scope.index_of_variable(name)

    def has_variable(self, name):
        return self.scope.has_variable(name)

    def push_variable(self, ast_elem):
        self.scope.put(ast_elem, "var")

    def get_variable(self, name):
        return self.scope.get_variable(name)

    def index_of_function(self, name):
        return self.scope.index_of_function(name)

    def has_function(self, name):
        return self.scope.has_function(name)

    def push_function(self, ast_elem):
        if self.scope.has_function(ast_elem.ident):
            self.push_scope(ast_elem.ident, ast_elem.ret_type)
            return True
        self.scope.put(ast_elem, "func")
        self.push_scope(ast_elem.ident, ast_elem.ret_type)
        for param in ast_elem.params:
            self.push_param(param)
        return True


    def get_function(self, name):
        return self.scope.get_function(name)

    def push_scope(self, name, return_type=None):
        parent = self.scope
        for child in parent.children:
            if child.name == name:
                self.scope = child
                return
        if return_type is None:
            return_type = parent.return_type
        self.scope = Scope(name, parent, return_type)
        parent.children.append(self.scope)

    def pop_scope(self):
        parent = self.scope.parent
        self.scope = parent
