from tokens import *
from error import *
from ast import *

class Parser:
    BINARY_LOOKUP_TABLE = {
        TOKEN_PLUS: "add",
        TOKEN_MINUS: "sub",
        TOKEN_STAR: "mul",
        TOKEN_FSLASH: "div"
    }

    def __init__(self, compiler=None, type_builder=None):
        self.compiler = compiler
        self.type_builder = type_builder
        self.module = ASTModule("root", None, compiler.src_path)

    def parse_binary_expression(self, tokens, lhs):
        if len(tokens) == 0:
            return False
        
        ##Start parsing for the operator token and the start parsing the rhs
        token = tokens[0] #We want to peek so we dont screw up the tokens list if we fail to parse for binary expression
        if token['kind'] not in Parser.BINARY_LOOKUP_TABLE:
            return None
        token = tokens.pop(0) #Now that we know that the token is a binary operator, we can pop it safely
        ir_kind = Parser.BINARY_LOOKUP_TABLE[token['kind']]
        rhs_token = tokens.pop(0)
        # debug_loc = {'line': token['loc']['line'], 'start_col': lhs['loc']['start_col'], 'end_col': rhs_token['loc']['end_col']}
        # display_debug(debug_loc, self.compiler.src_path, self.compiler.src_str.splitlines()[lhs['loc']['line']], "Parsing binary expression")
        rhs_parse_result = self.parse_expression(rhs_token, tokens)
        if not rhs_parse_result:
            loc = token['loc']
            display_error(
                loc, 
                self.compiler.src_path, 
                self.compiler.src_str.splitlines()[loc['line']], 
                "Expected integer as right hand side of binary operator but instead found `%s`" % token['value']
            )
            return None
        op_ir_elem = ASTBinary(ir_kind, token['loc'], lhs, rhs_parse_result, None) 
        return op_ir_elem

    def parse_array_get(self, array_name_token, tokens):
        array_index = self.parse_array_index(array_name_token, tokens)
        if not array_index:
            return array_index

        return ASTArrayGet(array_name_token['loc'], array_index)

    def parse_function_call(self, next_token, tokens):
        if len(tokens) == 0:
            ##NOTE: This should never happen!!
            loc = next_token['loc']
            display_error(
                loc, 
                self.compiler.src_path, 
                self.compiler.src_str.splitlines()[loc['line']], 
                "Expected '(' but found EOF"
            )
            return False
        if next_token['kind'] != TOKEN_WORD:
            return False
        func_ident = next_token['value']
        call_loc = next_token['loc']

        peek = tokens[0]
        if peek['kind'] != TOKEN_LPAREN:
            return False
        ignored = tokens.pop(0)
        next_token = tokens.pop(0)

        args = [] #[]ASTExpression
        while next_token['kind'] != TOKEN_RPAREN:
            expr_result = self.parse_expression(next_token, tokens)
            if not expr_result:
                return expr_result
            args.append(expr_result)

            peek = tokens[0]
            if peek['kind'] != TOKEN_COMMA:
                if peek['kind'] != TOKEN_RPAREN:
                    loc = peek['loc']
                    display_error(
                        loc, 
                        self.compiler.src_path, 
                        self.compiler.src_str.splitlines()[loc['line']], 
                        "Expected either ',' or ')'"
                    )
                    return False
                else:
                    tokens.pop(0)
                    break
            comma_token = tokens.pop(0)
            next_token = tokens.pop(0)
        
        return ASTFunCall(call_loc, func_ident, args)
        
    ##Parse an integer or return None
    def parse_integer(self, next_token, tokens):
        if next_token['kind'] != TOKEN_INT:
            return None
        return ASTInteger("int", next_token['loc'], next_token['value'])

    ##Parse a reference expression. This is in the form of:
    ##  ref <expr>
    ##  Example:
    ##      thing = some_func(5)
    ##      thing_ref = ref thing       <-- Reference expression
    def parse_reference(self, next_token, tokens):
        if next_token['kind'] != TOKEN_WORD:
            return None
        if next_token['value'] != "ref":
            return None
        refee_token = tokens.pop(0)
        if refee_token['kind'] != TOKEN_WORD:
            ##TODO: Add other conditions for reference condidates?
            display_error(
               refee_token['loc'],
               self.compiler.src_path,
               self.compiler.src_str.splitlines()[refee_token['loc']['line']],
               "Expected an identifier"
            )
            return None
        return ASTRef(next_token['loc'], refee_token['value'])

    def parse_variable_copy(self, next_token, tokens):
        if next_token['kind'] != TOKEN_WORD:
            return None
        return ASTVarCopy(next_token['loc'], next_token['value'])

    def parse_char_literal(self, next_token, tokens):
        if next_token['kind'] != TOKEN_CHAR:
            return None
        return ASTCharLiteral(next_token['loc'], next_token['value'])

    def parse_array_literal(self, next_token, tokens):
        if next_token['kind'] != TOKEN_LSQUARE:
            return False

        elems = []
        loc = next_token['loc']
        next_token = tokens.pop(0)
        while len(tokens) > 0 and next_token['kind'] != TOKEN_RSQUARE:
            elem_expr = self.parse_expression(next_token, tokens)
            if not elem_expr:
                return elem_expr
            elems.append(elem_expr)

            next_token = tokens.pop(0)
            if next_token['kind'] != TOKEN_COMMA and next_token['kind'] != TOKEN_RSQUARE:
                loc = next_token['loc']
                display_error(
                    loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[loc['line']],
                    "Expected either ',' or ']' in array literal"
                )
                return None
            if next_token['kind'] == TOKEN_COMMA:
                next_token = tokens.pop(0)

        return ASTArrayLiteral(loc, len(elems), None, elems)

    ##Parse an expression. This can be an integer literal, a variable copy/reference, a function call, or an array literal init.
    def parse_expression(self, next_token, tokens):
        ##Parse for an integer literal and try to parse for a binary expression
        parse_result = self.parse_integer(next_token, tokens)
        if parse_result:
            binary_parse_result = self.parse_binary_expression(tokens, parse_result) 
            if binary_parse_result:
                return binary_parse_result
            return parse_result

        ##Parse for a reference and try to parse for a binary expression
        parse_result = self.parse_reference(next_token, tokens)
        if parse_result:
            binary_parse_result = self.parse_binary_expression(tokens, parse_result) 
            if binary_parse_result:
                return binary_parse_result
            return parse_result


        ##Parse for a function call and try to parse for a binary expression
        parse_result = self.parse_function_call(next_token, tokens)
        if parse_result:
            binary_parse_result = self.parse_binary_expression(tokens, parse_result) 
            if binary_parse_result:
                return binary_parse_result
            return parse_result
        
        parse_result = self.parse_char_literal(next_token, tokens)
        if parse_result:
            binary_parse_result = self.parse_binary_expression(tokens, parse_result) 
            if binary_parse_result:
                return binary_parse_result
            return parse_result

        ##Array literals are not candidiates for being part of a binary expression
        parse_result = self.parse_array_literal(next_token, tokens)
        if parse_result:
            return parse_result

        ##Try to parse for an array get
        ##  Example: num = arr[2]
        parse_result = self.parse_array_get(next_token, tokens)
        if parse_result:
            return parse_result
        ##Parse for a variable copy and try to parse for a binary expression
        parse_result = self.parse_variable_copy(next_token, tokens)
        if parse_result:
            binary_parse_result = self.parse_binary_expression( tokens, parse_result) 
            if binary_parse_result:
                return binary_parse_result
            return parse_result     
        
        return None

    def display_unavailable_types(self, loc):
        display_info(
            loc,
            self.compiler.src_path,
            self.compiler.src_str.splitlines()[loc['line']],
            "Ttuples, dictionaries, and vectors are not yet supported"
        )

    def parse_array_type(self, array_start_tk, tokens):
        if array_start_tk['kind'] != TOKEN_LSQUARE:
            return False
        array_size_tk = tokens.pop(0)
        if array_size_tk['kind'] != TOKEN_INT:
            loc = array_size_tk['loc']
            display_error(
                loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[loc['line']],
                "Expected an array size"
            )
            return None
        array_end_tk = tokens.pop(0)
        if array_end_tk['kind'] != TOKEN_RSQUARE:
            loc = array_end_tk['loc']
            display_error(
                loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[loc['line']],
                "Expected ']'"
            )
            return None
        array_ty = self.parse_type(tokens)
        if not array_ty:
            return None
        return self.type_builder.create_array(
            array_size_tk['value'],
            array_ty,
            consolidate_locations(
                array_start_tk['loc'], 
                array_ty.loc
            )
        )
        

    def parse_type(self, tokens):
        ty_name_tk = tokens.pop(0)
        if ty_name_tk['kind'] != TOKEN_WORD:
            array_type = self.parse_array_type(ty_name_tk, tokens)
            if array_type is None:
                return None
            elif array_type is not False:
                return array_type
            else:
                loc = ty_name_tk['loc']
                display_error(
                    loc, 
                    self.compiler.src_path, 
                    self.compiler.src_str.splitlines()[loc['line']], 
                    "Expected type identifier for variable type annotation"
                )
                self.display_unavailable_types(loc)
                return None
            ##TODO: Add tuple, dictionary, etc parsing
            
        ty_name = ty_name_tk['value']
        if ty_name == "ref":
            ref_ty = self.parse_type(tokens)
            if not ref_ty:
                return None
            ty = self.type_builder.create_ref(
                ref_ty, 
                consolidate_locations(
                    ty_name_tk['loc'], 
                    ref_ty.loc
                )
            )
        else:
            ty = self.type_builder.create_primitive(ty_name, ty_name_tk['loc'])
        return ty


    def parse_variable(self, ident_token, tokens):
        ##Check if we can even parse for a variable
        if ident_token['kind'] != TOKEN_WORD:
            return False
        ##Even if we parse for a word token, it might not be a variable, it could be a function call or a reference etc
        peek = tokens[0]
        if peek['kind'] != TOKEN_COLON and peek['kind'] != TOKEN_EQ:
            return None
        next_token = tokens.pop(0)
        ty_name = self.type_builder.create_infer(next_token['loc'])
        if next_token['kind'] == TOKEN_COLON:
            ty_name = self.parse_type(tokens)
            if ty_name is None:
                return False
            next_token = tokens.pop(0)
        
        if next_token['kind'] != TOKEN_EQ:
            loc = next_token['loc']
            display_error(
                loc, 
                self.compiler.src_path, 
                self.compiler.src_str.splitlines()[loc['line']], 
                "Expected '=' in variable declaration but instead found %s" % next_token['value']
            )
            return False
        expr_token = tokens.pop(0)
        expr_ir = self.parse_expression(expr_token, tokens)
        if not expr_ir:
            return False
        return ASTVariable(ident_token['value'], ident_token['loc'], ty_name, expr_ir)

    ##Parse for an array index, this can either be an array set or get
    ##Example:
    ##  arr[index] = other[index]
    def parse_array_index(self, token, tokens):
        ##Check if the array index starts with a word aka array variable identifier
        if token['kind'] != TOKEN_WORD:
            return None
        ##'[' token
        next_token = tokens[0]
        if next_token['kind'] != TOKEN_LSQUARE:
            return False
        next_token = tokens.pop(0)
        ##Index expression token
        next_token = tokens.pop(0)

        expr_result = self.parse_expression(next_token, tokens)
        if not expr_result:
            return None
        ##']' token
        next_token = tokens.pop(0)
        if next_token['kind'] != TOKEN_RSQUARE:
            loc = next_token['loc']
            display_error(
                loc, 
                self.compiler.src_path, 
                self.compiler.src_str.splitlines()[loc['line']], 
                "Expected ']'"
            )
            return None
        return ASTArrayIndex(token['loc'], token['value'], expr_result)

    def parse_array_set(self, token, tokens):
        ##Parse for the array index lhs of the set statement
        array_index = self.parse_array_index(token, tokens)
        if not array_index:
            return array_index

        ##Parse for the '='
        next_token = tokens.pop(0)
        if next_token['kind'] != TOKEN_EQ:
            loc = next_token['loc']
            display_error(
                loc, 
                self.compiler.src_path, 
                self.compiler.src_str.splitlines()[loc['line']], 
                "Expected `=`"
            )
            return None

        ##Parse for the expression rhs of the set statement
        next_token = tokens.pop(0)
        expr = self.parse_expression(next_token, tokens)
        if not expr:
            return None
        ##Return with the ASTArraySet object
        return ASTArraySet(
                consolidate_locations(
                    token['loc'],
                    expr.loc
                ),
                array_index,
                expr
            )

    def parse_function(self, token, tokens):
        ##Parse for the 'def' keyword
        if token['value'] != "def":
            return False
        ##Parse for the function/proc identifier
        ident_token = tokens.pop(0)
        if ident_token['kind'] != TOKEN_WORD:
            loc = ident_token['loc']
            display_error(
                    loc, 
                    self.compiler.src_path, 
                    self.compiler.src_str.splitlines()[loc['line']], 
                    "Expected an identifier in function definition"
                )
            return None
        
        ident = ident_token['value']
        ident_loc = ident_token['loc']
        args_start_token = tokens.pop(0)
        if args_start_token['kind'] != TOKEN_LPAREN:
            loc = args_start_token['loc']
            display_error(loc, self.compiler.src_path, self.compiler.src_str.splitlines()[loc['line']], "Expected '(' in function definition")
            return None

        ##Start parsing for function parameters
        next_token = tokens.pop(0)
        params = [] #[]ASTParam
        while next_token['kind'] != TOKEN_RPAREN:
            if next_token['kind'] != TOKEN_WORD:
                loc = args_start_token['loc']
                display_error(loc, self.compiler.src_path, self.compiler.src_str.splitlines()[loc['line']], "Expected identifier for function argument in function definition")
                return None
            colon = tokens.pop(0)
            if colon['kind'] != TOKEN_COLON:
                loc = colon['loc']
                display_error(loc, self.compiler.src_path, self.compiler.src_str.splitlines()[loc['line']], "Expected ':' for function argument in function definition")
                return None

            ##Parse the type annotation of the param
            ty = self.parse_type(tokens)
            if not ty:
                return None

            params.append(
                    ASTFuncParam(
                        next_token['loc'],
                        next_token['value'],
                        ty
                    )
                )
            ##Check if there are more params to be expected
            ##  If we find a comma, then we should attempt to parse another parameter 
            ## otherwise, check if we are at the end of the list of params
            peek = tokens[0]
            if peek['kind'] != TOKEN_COMMA:
                if peek['kind'] == TOKEN_WORD:
                    loc = peek['loc']
                    display_error(loc, self.compiler.src_path, self.compiler.src_str.splitlines()[loc['line']], "Expected ',' in function argument list after arg identifier")
                    return None
                elif peek['kind'] == TOKEN_RPAREN:
                    tokens.pop(0)
                    break
                else:
                    loc = peek['loc']
                    display_error(loc, self.compiler.src_path, self.compiler.src_str.splitlines()[loc['line']], "Unexpected token, expected either ',' or ')'")
                    return None
                    
            comma_token = tokens.pop(0)
            next_token = tokens.pop(0)
        next_token = tokens.pop(0)
        ##Parse the return type
        ret_type = self.type_builder.create_infer(next_token['loc'])
        if next_token['kind'] == TOKEN_MINUS:
            next_token = tokens.pop(0)
            if next_token['kind'] != TOKEN_RANGLE:
                loc = next_token['loc']
                display_error(
                    loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[loc['line']],
                    "Expected '>' in function return type syntax"
                )
                display_info(
                    loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[loc['line']],
                    "Function return type syntax is `def ... -> `type`:"
                )
                return None
            ret_type = self.parse_type(tokens)
            if ret_type is None:
                return None
            next_token = tokens.pop(0)

        if next_token['kind'] != TOKEN_COLON:
            loc = next_token['loc']
            display_error(
                loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[loc['line']],
                "Expected ':'"
            )
            return False

        ##Parse the body of the function
        body = [] #[]ASTStatement
        peek = tokens[0]
        start_indent = next_token['loc']['indent']
        while peek['loc']['indent'] > start_indent and len(tokens) > 0:
            ##Parse a single statement for the body of the function
            statement = self.parse_statement(tokens)
            if not statement:
                return result
            body.append(statement)
            if len(tokens) > 0:
                peek = tokens[0]

        
        return ASTFunction(
                ident_loc,
                ident,
                len(params),
                params,
                ret_type,
                body
            )
        

    def parse_return_statement(self, token, tokens):
        if token['kind'] != TOKEN_WORD:
            return False
        
        if token['value'] != "return":
            return False
        
        expr_token = tokens.pop(0)
        expr_result = self.parse_expression(expr_token, tokens)
        if not expr_result:
            return None
        return ASTReturn(token['loc'], expr_result)

    def parse_statement(self, tokens=None):
        token = tokens.pop(0)
        old_tokens_state = tokens
        result = None
        if token['kind'] == TOKEN_WORD:
            ##Parse a function
            result = self.parse_function(token, tokens)
            if result is None:
                return None
            elif result is not False:
                return result
            tokens = old_tokens_state

            #Parse return statement
            #   This comes before parsing a variable because otherwise,
            #   the parser will accidentally treat the "return" token as
            #   a variable identifier
            result = self.parse_return_statement(token, tokens)
            if result is None:
                return False
            elif result is not False:
                return result
            tokens = old_tokens_state
            
            #Parse an array set
            #   This comes before the variable because otherwise the variable
            #   parser may throw an error, this could be changed in the future
            #   but for now I am fine with the variable parser being the last
            #   in the parse chain
            result = self.parse_array_set(token, tokens)
            if result is None:
                return None
            elif result is not False:
                return result
            tokens = old_tokens_state

            #Parse a variable
            result = self.parse_variable(token, tokens)
            if result is None:
                return None
            elif result is not False:
                return result

        else:
            result = self.parse_expression(token, tokens)
            if not result:
                display_error(
                    token['loc'], 
                    self.compiler.src_path, 
                    self.compiler.src_str.splitlines()[token['loc']['line']],
                    "Unrecognized statement"
                )
                return None
        return result

    def parse(self, tokens=[]):
        ##Main parsing phase for non-functions
        _start_body = []
        while len(tokens) > 0:
            result = self.parse_statement(tokens)
            if not result:
                print("Failed to parse a statement")
                return None
            if isinstance(result, ASTFunction):
                self.module.body.append(result)
            else:
                _start_body.append(result)

        start_func = ASTFunction(
            default_token_location,
            "_start",
            0,
            [],
            self.type_builder.create_empty(default_token_location),
            _start_body
        )
        self.module.body.append(start_func)
        return self.module


