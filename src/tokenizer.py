from iota import iota
from tokens import *

class Tokenizer:
    def __init__(self, compiler=None):
        self.compiler = compiler

    def find_col(self, line, start, predicate):
        while start < len(line) and not predicate(line[start]):
            start += 1
        if start == len(line):
            return (None, start)
        else:
            return (line[start], start)

    def generate_lexemes_for_line(self, line_index=None, line=None):
        index = 0
        lexemes = []
        global puncts
        indent_level = 0
        count_indent = True
        space_count = 0
        while index < len(line):
            start_col = index
            ## Here we want to separate the lexemes into words, integers, and punctuation by chcking if the char is either a space or a punctuation
            (char, end_col) = self.find_col(line, start_col, lambda c: c.isspace() or c == '\n' or c in puncts and c != '_')
            if start_col == end_col:
                if char == None:
                    break
                elif char == '':
                    display_warning(
                        {'line': line_index, 'start_col': start_col, 'end_col': end_col, 'indent': indent_level},
                        self.compiler.src_path,
                        line,
                        "Attempted to tokenize line but found an empty char. This is a compiler bug!"
                    )
                    continue
                if char == ' ':
                    index = end_col + 1
                    
                    if count_indent is True:
                        space_count += 1
                        if space_count % 4 == 0:
                            indent_level = int(space_count / 4)
                    continue
                elif char == "'":
                    index = end_col + 1
                    if index >= len(line):
                        display_error(
                            {'line': line_index, 'start_col': index-1, 'end_col': index-1, 'indent': indent_level},
                            self.compiler.src_path,
                            line,
                            "Incomplete char literal: found EOL"
                        )
                        return None
                    c = line[index]
                    index += 1
                    if line[index] != "'":
                        display_error(
                            {'line': line_index, 'start_col': index, 'end_col': index, 'indent': indent_level},
                            self.compiler.src_path,
                            line,
                            "Expected single char between two `'` but instead found `string`"
                        )
                        return None
                    lexemes.append({'data': f"'{c}'", 'start': start_col, 'end': index+1, 'indent': indent_level})
                    index += 1
                    continue
                if count_indent is True:
                    count_indent = False
                lexemes.append({'data': char, 'start': start_col, 'end': end_col, 'indent': indent_level})
                index = end_col + 1
            else:
                count_indent = False
                lexeme = line[start_col:end_col]
                lexemes.append({'data': lexeme, 'start': start_col, 'end': end_col, 'indent': indent_level})
                index = end_col
        return lexemes

    def lex_file(self):
        tokens = []
        lines = self.compiler.src_str.split('\n')
        for index, line in enumerate(lines):
            lexemes = self.generate_lexemes_for_line(index, line)
            if lexemes is None:
                return None
            for lexeme in lexemes:
                lex = lexeme['data']
                token = {'loc': {'line': index, 'start_col': lexeme['start'], 'end_col': lexeme['end'], 'indent': lexeme['indent']}, 'kind': TOKEN_UNKNOWN, 'value': lexeme['data']}
                if lex == '=':
                    token['kind'] = TOKEN_EQ
                elif lex == '+':
                    token['kind'] = TOKEN_PLUS
                elif lex == '-':
                    token['kind'] = TOKEN_MINUS
                elif lex == '*':
                    token['kind'] = TOKEN_STAR
                elif lex == '/':
                    token['kind'] = TOKEN_FSLASH
                elif lex == '\\':
                    token['kind'] = TOKEN_BSLASH
                elif lex == '(':
                    token['kind'] = TOKEN_LPAREN
                elif lex == ')':
                    token['kind'] = TOKEN_RPAREN
                elif lex == ',':
                    token['kind'] = TOKEN_COMMA
                elif lex == ':':
                    token['kind'] = TOKEN_COLON
                elif lex == '>':
                    token['kind'] = TOKEN_RANGLE
                elif lex == '<':
                    token['kind'] = TOKEN_LANGLE
                elif lex == '[':
                    token['kind'] = TOKEN_LSQUARE
                elif lex == ']':
                    token['kind'] = TOKEN_RSQUARE
                elif lex.isnumeric():
                    token['kind'] = TOKEN_INT
                    try:
                        token['value'] = int(lex)
                    except Exception:
                        print("[ERROR]: Failed to convert token lexeme %s to int" % lex)
                        return None
                else:
                    if lex.startswith("'") and lex.endswith("'"):
                        token['kind'] = TOKEN_CHAR
                        token['value'] = lex[1]
                    else:
                        token['kind'] = TOKEN_WORD
                        token['value'] = lex
                tokens.append(token)
        return tokens
