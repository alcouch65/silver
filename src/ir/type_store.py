from .ir import *
from error import *
from .lowering import *
from .ir_list import IRList
from tokens import default_token_location

class TypeStore:
    def __init__(self, compiler, type_builder, module):
        self.module = module
        self.compiler = compiler
        self.type_builder = type_builder
        self.tuples_count = 0
        self.arrays_count = 0

    def get_pointer_to_type_ref(self, ty_name, loc):
        type_ref = self.get_type_ref_by_name(ty_name, loc)
        if not type_ref:
            return None
        return IRPointerType(type_ref, loc)

    def get_type_ref_by_name(self, name, loc):
        ty_def = self.module.types.find(lambda ty: ty.type_name == name)
        if ty_def is None:
            return None
        return IRTypeRef(ty_def, loc)

    def collect_pointers(self):
        return self.module.types.filter(lambda ty: isinstance(ty, IRPointerType))

    def collect_arrays(self):
        return self.module.types.collect(lambda ty: ty.type_list.length == 1).filter(lambda ty: isinstance(ty.type_list.first(), IRArrayType)).transform(lambda ty: ty.type_list.first())

    def find_array(self, size, inner_type):
        return self.collect_arrays() \
                .find(
                    lambda arr: \
                        arr.size == size \
                        and self.compare_types(
                            arr.inner_type, 
                            inner_type
                        )
                    )

    ##This method is more complex because tuples are not unityped like arrays are, so we have to make
    ##  sure that all the given inner types match a given defined tuple
    def find_tuple(self, size, inner_types):
        ##Step 1. collect all the tuples
        ##Step 2. find a tuple that matched the following conditions
        ##  Step 3. zip current tuple with given inner_types
        ##  Step 4. Get comparison results paired with zipped inner types by comparing each inner type
        ##  Step 5. Collect all the unmatched types
        ##  Step 6. Check if empty
        ##      Note: If .empty() == True, then we want to return that elements from find method
        ##              otherwise, continue to next iteration until no more
        return self.module.types.collect(lambda ty: ty.type_list.length == size) \
                .find(lambda ty: \
                    ty.type_list.zip(inner_types) \
                    .get_compares(lambda t1, t2: self.compare_types(t1, t2)) \
                    .collect(lambda match: match[0] == False) \
                    .empty() \
                )

    def get_type_ref_by_ast(self, ast):
        if ast.kind == "primitive":
            if ast.name == "ref":
                refee_type = ast.data['type']
                refee_type = self.get_type_ref_by_ast(refee_type)
                if not refee_type:
                    display_error(
                        ast.loc,
                        self.compiler.src_path,
                        self.compiler.src_str.splitlines()[ast.loc['line']],
                        f"Could not find type {ast.data['type'].dump()}"
                    )
                    return None
                return IRPointerType(refee_type, ast.loc)
            return self.get_type_ref_by_name(ast.name, ast.loc)
        elif ast.kind == "aggregate":
            if ast.name == "array":
                size = ast.data['size']
                ##Get the inner type as a type reference
                inner_type = self.get_type_ref_by_ast(ast.data['type'])
                if inner_type is None:
                    ##FIXME: Show error
                    return None
                ##Find an array with the matching size and inner type
                array = self.find_array(size, inner_type)
                if not array:
                    array = IRTypeDef(f"array.{self.arrays_count}", IRList([IRArrayType(size, inner_type, ast.loc)]), ast.loc)
                    self.module.types.push(array)
                return IRTypeRef(array, ast.loc)

            elif ast.name == "tuple":
                ##Transform all the ast nodes of the tuple inner types to type refs
                inner_types = IRList(ast.data['type']) \
                        .transform(lambda ty: \
                            self.get_type_ref_by_ast(ty)
                        )
                ##Check if we have failed to get a type reference from the ast node
                failed = inner_types.get_compares(lambda ty: ty is not None) \
                            .filter(lambda tup: tup[0] is True) \
                            .not_empty()
                ##If we have failed for any of them, then we want to iterate through them
                ##  and display an error at that location
                if failed:
                    failed.iterate(
                        lambda failed, ty: \
                            display_error(
                                ty.loc, 
                                self.compiler.src_path, 
                                self.compiler.src_str.splitlines()[ty.loc['line']], 
                                "Could not find type definition"
                            )
                        )
                    return None
                ##Now we want to find a tuple with these matches attributs
                tupl = self.find_tuple(ast.data['size'], inner_types)
                if not tupl:
                    display_error(
                        ast.loc, 
                        self.compiler.src_path, 
                        self.compiler.src_str.splitlines()[ast.loc['line']], 
                        "Could not find tuple definition"
                    )
                    return None
                return IRTypeRef(tupl)
        self.type_not_impl_yet(ast.name, ast.loc)
        return None

    def generate_primtive_integers(self):
        self.module.types.push(IRTypeDef("u8", IRList([]), default_token_location))
        
        u16 = self.convert_primtive(self.type_builder.create_u16(default_token_location))
        if not u16:
            return False
        self.module.types.push(u16)
        
        u32 = self.convert_primtive(self.type_builder.create_u32(default_token_location))
        if not u32:
            return False
        self.module.types.push(u32)

        u64 = self.convert_primtive(self.type_builder.create_u64(default_token_location))
        if not u64:
            return False
        self.module.types.push(u64)
        return True

    def primitive_conversion_error(self, prim_name, loc):
        display_error(
            None,
            self.compiler.src_path,
            self.compiler.src_str.splitlines()[loc['line']],
            f"Attempted to create primtive type reference but could not find {prim_name} primitive definition; This is a bug in the compiler"
        )

    def type_not_impl_yet(self, ty_name, loc):
        display_error(
            loc,
            self.compiler.src_path,
            self.compiler.src_str.splitlines()[loc['line']],
            f"{ty_name} is not yet implemented!"
        )

    ##FIXME: Here when we generate the u8 type def, should an empty type list be considered opaque?
    def convert_primtive(self, ty):
        if ty.name == "u8":
            return IRTypeDef("u8", IRList([]), ty.loc)
        elif ty.name == "u16":
            u8 = self.get_type_ref_by_name("u8", ty.loc)
            if not u8:
                self.primitive_conversion_error("u8", ty.loc)
                return None
            return IRTypeDef("u16", IRList([u8, u8]), ty.loc)
        elif ty.name == "u32":
            u16 = self.get_type_ref_by_name("u16", ty.loc)
            if not u16:
                self.primitive_conversion_error("u16", ty.loc)
                return None
            return IRTypeDef("u32", IRList([u16, u16]), ty.loc)
        elif ty.name == "u64":
            u32 = self.get_type_ref_by_name("u32", ty.loc)
            if not u32:
                self.primitive_conversion_error("u32", ty.loc)
                return None
            return IRTypeDef("u64", IRList([u32, u32]), ty.loc)
        elif ty.name == "empty":
            return IRTypeDef("empty", IRList([]), ty.loc)

        self.type_not_impl_yet(ty.name, ty.loc)
        return None

    def get_existing_type(self, typedef):
        return self.module.types.find(lambda ty: self.compare_types(typedef, ty))

    ##This will take a tuple AST node and convert
    ##  it to an tuple IRTypeDef
    def convert_tuple(self, ty):
        ##TODO: Generalize IRList so that we can do ty.data['types'].transform(...)
        types = IRList(ty.data['types'])
        refs = types.transform(lambda ty: self.get_type_ref_by_ast(ty))
        typedef = IRTypeDef(f"tuple.{self.tuples_count}",
                refs,
                ty.loc
            )
        same_type = self.get_existing_type(typedef)
        if same_type:
            return same_type

        self.tuples_count += 1
        return typedef

    def get_array_def_by_ast(self, ty):
        arr_size = ty.data['size']
        inner_type = self.get_type_ref_by_ast(ty.data['type'])
        return self.collect_arrays() \
                .collect(lambda arr: size == arr.size) \
                .find(lambda arr: self.compare_types(inner_type, arr.inner_type))

    def convert_array(self, ty):
        arr = self.get_array_def_by_ast(ty)
        if arr:
            return arr

        ##If we haven't found a previously existing array type
        ##  then we will just make a new one
        elem_type = self.convert_type_def(ty.data['type'])
        typdef = IRTypeDef(
            f"array.{self.arrays_count}",
            IRList([IRArrayType(ty.data['size'], elem_type)])
        )
        self.arrays_count += 1
        return typdef

    def convert_aggregate(self, ty):
        if ty.name == "array":
            return self.convert_array(ty)
        elif ty.name == "tuple":
            return self.convert_tuple(ty)
        else:
            primitive_not_impl_yet(ty.name, ty.loc)
            return None

    def is_primitive(self, ty):
        return ty.kind == "primitive"

    def is_aggregate(self, ty):
        return ty.kind == "aggregate"

    def convert_type_def(self, ty):
        if ty.kind == "primitive":
            return self.convert_primtive(ty)
        elif ty.kind == "aggregate":
            return self.convert_aggregate(ty)
        else:
            primitive_not_impl_yet(ty.name, ty.loc)
            return None

    def convert_func_params_to_tuple(self, params, loc):
        tuple_types = []
        for param in params:
            param_ty = param.type
            tuple_types.append(param_ty)

        tuple_type = self.type_builder.create_tuple(
                tuple_types, 
                len(params),
                loc
            )
        params_type = self.push_type_def(tuple_type)
        return IRPointerType(params_type, loc)

    def convert_func_retvals_to_tuple(self, retvals):
        assert retvals is not None
        return IRPointerType(self.push_type_def(retvals), retvals.loc)

    def convert_func_call_args_to_tuple(self, call_args, loc):
        tuple_types = []
        for arg in call_args:
            arg_ty = arg.type
            tuple_types.append(arg_ty)

        tuple_type = self.type_builder.create_tuple(
                tuple_types, 
                len(tuple_types),
                loc
            )
        return self.push_type_def(tuple_type)

    def is_opaque(self, ty):
        return ty.type_list.empty

    def check_array_type(self, arr_type):
        return isinstance(arr_type, tuple)

    def compare_array_types(self, arr1, arr2):
        ##Ensure that the two array types really are array types
        if not self.check_array_type(arr1):
            return False
        if not self.check_array_type(arr2):
            return False
        ##Destructure the two array types
        (arr1_size, arr1_type) = arr1
        (arr2_size, arr2_type) = arr2
        ##Check if they are the same size
        if arr1_size != arr2_size:
            return False
        ##Compare the inner types
        return self.compare_types(arr1_type, arr2_type)
    
    def compare_types(self, ty1, ty2):
        if ty1 is None:
            return False
        if ty2 is None:
            return False
        if ty1.type_name == ty2.type_name:
            return True
        ##Check if either of them are opaque
        if self.is_opaque(ty1) or self.is_opaque(ty2):
            return False

        for idx, (ty1_elem, ty2_elem) in ty1.type_list.zip(ty2.type_list).as_enum():
            if ty1_elem is None:
                continue
            if ty2_elem is None:
                continue
            ##Check if they are arrays
            if self.check_array_type(ty1_elem):
                ##If the first type element is an array type 
                ##  but the second one isn't, then this check fails
                if not self.check_array_type(ty2_elem):
                    return False
                if not self.compare_array_types(ty1_elem, ty2_elem):
                    return False

            ##If the types are not arrays, then we can compare their type definitions
            ty1_elem_def = self.get_type_def_by_name(ty1_elem.type_name)
            if not ty1_elem_def:
                return False

            ty2_elem_def = self.get_type_def_by_name(ty2_elem.type_name)
            if not ty2_elem_def:
                return False
            if not self.compare_types(ty1_elem_def, ty2_elem_def):
                return False
        return True

    def check_type_exists(self, typ):
        found = self.module.types \
            .transform(lambda ty: self.get_type_def_by_name(ty.type_name)) \
            .get_compares(lambda ty: ty is not True) \
            .filter(lambda elem: elem[0] is True) \
            .transform(lambda elem: elem[1]) \
            .find(lambda ty: self.compare_types(ty, typ))
                
        #for ty in self.module.types:
        #    tydef = self.get_type_def_by_name(ty.type_name)
        #    if not tydef:
        #        return False
        #    if self.compare_types(tydef, typ):
        #        return True
        return found is not None

    def get_type_list(self, type_ref):
        type_def = self.get_type_def_by_name(type_ref.type_name)
        if not type_def:
            return None
        return type_def.type_list

    def get_type_at_index_from_list(self, type_ref, index):
        type_list = self.get_type_list(type_ref)
        if index >= len(type_list):
            return None
        return type_list[index]

    def get_type_def_by_ast(self, ty_ast):
        ty_def = self.iterate_types(ty_def, self.compare_types)
        return ty_def

    def get_type_def_by_name(self, typ_name):
        return self.module.types.find(lambda ty: ty.type_name == typ_name)
        for typ in self.module.types:
            if typ.type_name == typ_name:
                return typ
        return None

    def push_type_def(self, ty):
        ir_ty = self.convert_type_def(ty)
        if ir_ty is None:
            return None
        if not self.check_type_exists(ir_ty):
            self.module.types.push(ir_ty)
        return IRTypeRef(ir_ty, ir_ty.loc)
