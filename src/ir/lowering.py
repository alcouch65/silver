from .ir import *
from .type_store import TypeStore
from error import *
from tokens import default_token_location
from ast import *


class StackFrame:
    def __init__(self, compiler, in_args_type, out_args_type, type_store):
        self.compiler = compiler
        self.variables = [] #List[IRVariable]
        self.body = [] #List[IRInstruction]
        self.stack_size = 0
        self.type_store = type_store
        ##FIXME: Store the location of the start of the stack frame?
        self.in_args = IRVariableRef("__in_args__", in_args_type, default_token_location)
        self.out_args = IRVariableRef("__out_args__", out_args_type, default_token_location)
        #self.in_args = self.createVariable("__in_args__", in_args_type, default_token_location)
        #self.out_args = self.createVariable("__out_args__", out_args_type, default_token_location)

    def createVariable(self, var_name, var_type, loc):
        alloc = IRAlloc(var_type, loc)
        #var_type_ptr = self.type_store.get_pointer_to_type_ref(var_type.ty_def.type_name, loc)
        #if not var_type_ptr:
        #    display_error(
        #        loc,
        #        self.compiler.src_path,
        #        self.compiler.src_str.splitlines()[loc['line']],
        #        f"Failed to get pointer to type {var_type.dump()}")
        #    return None
        #print(f"var_type_ptr = {var_type_ptr.dump()}")
        var_type_ptr = IRPointerType(var_type, loc)
        var = IRVariable(var_name, var_type_ptr, alloc, loc)
        self.variables.append(var)
        self.body.append(var)
        var_ref = IRVariableRef(var_name, var_type_ptr, loc)
        return var_ref

    def createVariableFromAST(self, var_ast):
        var_name = var_ast.ident
        var_type_ast = var_ast.type
        var_type = self.type_store.get_type_ref_by_ast(var_type_ast)
        print(f"Var type = {var_type.dump()}")
        if not var_type:
            display_error(
                var_ast.loc,
                self.compiler.src_path,
                self.compiler.src_str[var_ast.loc['line']],
                f"Could not find type definition {var_type_ast.dump()}"
            )
            return None
        return self.createVariable(var_name, var_type, var_ast.loc)

    def storeVariable(self, var, var_expr, loc):
        index = IRConstant(self.type_store.get_type_ref_by_name("u8", loc), 0, loc)
        return self.storeVariableAtIndex(var, var_expr, index, loc)

    def storeVariableAtIndex(self, var, var_expr, index, loc):
        if isinstance(index, int):
            index = IRConstant(self.type_store.get_type_ref_by_name("u8", loc), index, loc)
        ir_set = IRSet(var, var_expr, index, loc)
        return ir_set

    def storeVariableWithName(self, var_name, var_expr, loc):
        ref = self.getVariableRef(var_name, loc)
        return self.storeVariable(ref, var_expr, loc)

    def storeVariableFromAST(self, var_ref_ast, var_expr):
        var_ref_name = var_ref_ast.name
        var_ref = self.getVariableRefFromAST(var_ref_ast)
        if not var_ref:
            display_error(
                var_ref_ast.loc,
                self.compiler.src_path, self.compiler.src_str.splitlines()[var_ref_ast.loc['line']],
                f"Could not get variable reference for variable {var_ref_name}"
            )
            return None
        return self.storeVariable(var_ref, var_expr, var_ref_ast.loc)

    def getVariable(self, var, loc):
        index = IRConstant(self.type_store.get_type_ref_by_name("u8", loc), 0, loc)
        return self.getVariableAtIndex(var, index, loc)

    def getVariableAtIndex(self, var, index, loc):
        if isinstance(index, int):
            index = IRConstant(self.type_store.get_type_ref_by_name("u8", loc), index, loc)
        return IRGet(var, index, loc)

    def getVariableWithName(self, var_name, loc):
        ref = self.getVariableRef(var_name, loc)
        return self.getVariable(ref, loc)

    def getVariableFromAST(self, var_ref_ast):
        var_ref_name = var_ref_ast.ident
        return self.getVariable(var_ref_name, var_expr, var_ref_ast.loc)

    def getVariableRef(self, var_name, loc):
        var = self.queryVariable(var_name)
        if not var:
            return None
        var_type = var.typ
        var_ref = IRVariableRef(var_name, var_type, loc)
        return var_ref

    def getVariableRefFromAST(self, var_ref_ast):
        var_name = var_ref_ast
        return self.getVariableRef(var_name, var_ref_ast.loc)


    def queryVariable(self, var_name):
        for var in self.variables:
            if var.var_name == var_name:
                return var

        return None
    
    def queryVariableOrElse(self, var_name, or_else):
        var = self.queryVariable(var_name)
        if not var:
            return or_else
        return var

    def get_arg_at_index(self, index, loc):
        get_index = IRConstant(self.type_store.get_type_ref("u8"), index)
        return IRGet(self.in_args, get_index, loc)

    def set_out_arg_at_index(self, index, value, loc):
        set_index = IRConstant(self.type_store.get_type_ref("u8", loc), index, loc)
        return IRSet(self.out_args, value, set_index, loc)

class ASTLowering:
    def __init__(self, compiler, ast, symbol_table, type_builder):
        self.compiler = compiler
        self.ast = ast
        self.symbol_table = symbol_table
        self.module = IRModule(ast.name, compiler.src_path)
        ##TODO: Add DataStore
        ##This is where all the IRType's will be stored
        self.type_store = TypeStore(compiler, type_builder, self.module)
        self.tempname_count = 0
        self.type_builder = type_builder

    def lower_function(self, func):
        in_type = self.type_store.convert_func_params_to_tuple(func.params, func.loc)
        if not in_type:
            return False
        out_type = self.type_store.convert_func_retvals_to_tuple(func.ret_type)
        frame = StackFrame(self.compiler, in_type, out_type, self.type_store) 
        for idx, param in enumerate(func.params):
            param_var = frame.createVariableFromAST(param)
            if not param_var:
                display_error(
                    param.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitlines()[param.loc['line']],
                    "Could not create parameter variable while lowering function")
                return False
            frame.storeVariable(param_var, frame.getVariableAtIndex(frame.in_args, idx, param.loc), param.loc)

        for stmt in func.body:
            stmt_ir = self.lower_local_statement(stmt, frame)
            if not stmt_ir:
                print(f"Failed to lower statement in function body:\n{stmt.dump()}")
                return False

        ir_routine = IRRoutine(func.ident, in_type, out_type, frame.body, func.loc)
        self.module.routines.append(ir_routine)
        return True

    def generate_const_index(self, index, loc):
        typ = self.type_store.get_type_ref_by_name("u8", loc)
        return IRConstant(typ, index, loc)

    def lower_integer_literal(self, frame, var, int_ast):
        ty = self.type_store.get_type_ref_by_ast(int_ast.type)
        if not ty:
            display_error(
                int_ast.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[int_ast.loc['line']],
                f"For some reason, could not get a type reference for cosntant type {int_ast.type.name}"
            )
            return None
        const = IRConstant(
                ty, 
                int_ast.value,
                int_ast.loc
            )
        if not var:
            return const
        return frame.storeVariable(var, const, int_ast.loc)

    def generate_stack_alloc_ir_and_type(self, typ, loc):
        ir_type_ref = self.type_store.get_type_ref(typ.name, typ.loc)
        ir_type = IRPointerType(ir_type_ref, typ.loc)

        ir_alloc = IRAlloc(ir_type_ref, loc)
        return (ir_alloc, ir_type)

    ##var_ast = ASTVariable
    ##frame = StackFrame
    def lower_variable(self, var_ast, frame):
        var = frame.createVariableFromAST(var_ast)
        if not var:
            return False
        expr = self.lower_expression(var, frame, var_ast.expr)
        if not expr:
            return False
        frame.body.append(expr)
        return True

    def lower_var_ref(self, var, frame: StackFrame, expr):
        expr_ref = frame.getVariableRef(expr.refee, expr.loc)
        if not var:
            return expr_ref
        var_ref = frame.getVariableRef(var.var_name, var.loc)
        if not var_ref:
            display_error(
                var_ast.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[var_ast.loc['line']],
                f"Failed to create variale reference from variable {var_ast.ident}"
            )
            return None
        ir_set = IRSet(var_ref, expr_ref, self.generate_const_index(0, default_token_location), expr.loc)
        return ir_set

    ##[var] is the variable that this expression is being assigned to
    ##[expr] is the variable being copied from
    def lower_var_copy(self, frame: StackFrame, var, expr):
        ir_get = frame.getVariableWithName(expr.ident, expr.loc)
        if not var:
            return ir_get
        return frame.storeVariable(var, ir_get, var.loc)
        
    def lower_binary_arith(self, var, frame, expr):
        ##We want these to have a None var because we want to try
        ##  to use them in the binary arith operation as an operand
        ##The methods for expressions are smart enough to know
        ##  that if var is None, then to just return its own expression IR
        ##  unless it needs to create a variable for storage
        ##  such as with arrays
        lhs = self.lower_expression(None, frame, expr.lhs)
        if not lhs:
            display_error(
                expr.lhs.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[expr.lhs.loc['line']],
                "Failed to lower left-hand side of binary expression"
            )
            return None
        
        rhs = self.lower_expression(None, frame, expr.rhs)
        if not rhs:
            display_error(
                expr.rhs.loc,
                self.compiler.src_path,
                self.compiler.src_str.splitlines()[expr.rhs.loc['line']],
                "Failed to lower right-hand side of binary expression"
            )
            return None

        if expr.name == "add":
            return IRAdd(lhs, rhs, var, expr.loc)
        elif expr.name == "sub":
            return IRSub(lhs, rhs, var, expr.loc)
        elif expr.name == "mul":
            return IRMul(lhs, rhs, var, expr.loc)
        elif expr.name == "div":
            return IRDiv(lhs, rhs, var, expr.loc)
        else:
            display_error(
                expr.loc,
                self.compiler.src_path,
                self.compiler.src_path.splitlines()[expr.loc['line']],
                "Unrecognized binary expression"
            )
            return None

    def lower_char_literal(self, frame: StackFrame, var, char):
        var_type = self.type_store.get_type_ref(char.type.name)
        if not var_type:
            display_error(
                    char.loc,
                    self.compiler.src_path,
                    self.compiler.src_str.splitline()[char.loc['line']],
                    "For some reason, u8 type is not defined. Try calling the type_store's generate_primitive_types method before doing this."
                )
            return None
        const = IRConst(var_type, char.value)
        if not var:
            return const
        return frame.storeVariable(var, const, var.loc)

    def create_temp_varname(self):
        name = f"{self.tempname_count}"
        self.tempname_count += 1
        return name

    def create_temp_variable(self, frame, var_type, loc):
        var_name = self.create_temp_varname()
        return frame.createVariable(var_name, var_type, loc)


    def lower_fun_call(self, var, frame, call):
        ##Return value
        ##We only wanna create a new one if we don't have a variable being assigned
        if not var:
            ret_type = self.type_store.convert_type_def(call.type)
            if not ret_type:
                return None
            var = self.create_temp_variable(frame, call.type, call.loc)
            if not var:
                return None
            frame.body.append(var)
        ##Call args
        call_args_type = self.type_store.convert_func_call_args_to_tuple(call.args, call.loc)
        if not call_args_type:
            return None
        call_args_var = self.create_temp_variable(frame, call_args_type, call.loc)
        if not call_args_var:
            return None
        #frame.body.append(call_args_var)
        
        for idx, arg in enumerate(call.args):
            arg_expr = self.lower_expression(None, frame, arg)
            ir_set_idx = self.generate_const_index(idx, arg.loc)
            frame.storeVariableAtIndex(call_args_var, arg_expr, ir_set_idx, arg.loc)

        return IRCall(call.func_name, call_args_var, var, call.loc)

    def lower_array_literal(self, var, frame, array):
        if not var:
            arr_type = self.type_store.push_type_def(array.type)
            var = self.create_temp_variable(frame, arr_type, array.loc)
            if not var:
                return None
        for idx, elem in enumerate(array.elements):
            expr = self.lower_expression(None, frame, elem)
            if not expr:
                return None
            frame.storeVariableAtIndex(var, expr, idx, var.loc)
        
        return var
        
    def lower_array_get(self, var, frame: StackFrame, expr):
        ast_index = expr.index
        index_expr = self.lower_expression(None, frame, ast_index.index_expr)
        index_var = frame.getVariableRef(ast_index.array_ident, expr.loc)
        if not index_var:
            return None
        ir_get = frame.getVariableAtIndex(index_var, index_expr, expr.loc)
        if not var:
            return ir_get
        return frame.storeVariable(var, ir_get, var.loc)

    def lower_array_set(self, frame: StackFrame, ast):
        index_expr = ast.index.index_expr
        index = self.lower_expression(None, frame, index_expr)
        if not index:
            print("Failed to lower array index expression")
            return False
        var = frame.getVariableRef(ast.index.array_ident, index_expr.loc)
        expr = self.lower_expression(None, frame, ast.expr)
        if not expr:
            print("Failed to lower array set expression")
            return False

        print("Successfully lowered array set statement, storing variable")
        return frame.storeVariableAtIndex(var, expr, index, ast.loc)

    def lower_expression(self, var, frame, expr):
        if isinstance(expr, ASTInteger):
            return self.lower_integer_literal(frame, var, expr)
        elif isinstance(expr, ASTBinary):
            return self.lower_binary_arith(var, frame, expr)
        elif isinstance(expr, ASTVarCopy):
            return self.lower_var_copy(frame, var, expr)
        elif isinstance(expr, ASTRef):
            return self.lower_var_ref(var, frame, expr)
        elif isinstance(expr, ASTCharLiteral):
            return self.lower_char_literal(frame, var, expr)
        elif isinstance(expr, ASTFunCall):
            return self.lower_fun_call(var, frame, expr)
        elif isinstance(expr, ASTArrayLiteral):
            return self.lower_array_literal(var, frame, expr)
        elif isinstance(expr, ASTArrayGet):
            return self.lower_array_get(var, frame, expr)
        display_error(
            expr.loc,
            self.compiler.src_path,
            self.compiler.src_str.splitlines()[expr.loc['line']],
            f"Unrecognized expression"
        )
        return None

    def lower_local_statement(self, stmt, frame):
        if isinstance(stmt, ASTVariable):
            return self.lower_variable(stmt, frame)
        if isinstance(stmt, ASTExpression):
            return self.lower_expression(stmt, frame)
        elif isinstance(stmt, ASTArraySet):
            return self.lower_array_set(frame, stmt)
        elif isinstance(stmt, ASTReturn):
            out_arg = frame.out_args
            expr = self.lower_expression(out_arg, frame, stmt.expr)
            if not expr:
                return None
            frame.body.append(expr)
            ##TODO: Recognized tuple literals and array literals?
            frame.body.append(IRReturn(stmt.loc))
            return True

        return False

    ##Here are the lowering of functions, structs, etc
    ##All top level variables are actually compiled into a main function for the module
    def lower_top_level_statement(self, stmt):
        if isinstance(stmt, ASTFunction):
            func_ir = self.lower_function(stmt)
            if not func_ir:
                return False
            return True
        return False
    
    def lower_ast(self):
        if not self.type_store.generate_primtive_integers():
            return None
        for stmt in self.ast.body:
            if not self.lower_top_level_statement(stmt):
                print(f"Failed to lower top level statement:\n    {stmt.dump()}")
                return None
        return self.module
