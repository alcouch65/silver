class IRList:
    def __init__(self, elems):
        self.elems = elems
        self.length = len(elems)

    def empty(self):
        return self.length == 0

    def not_empty(self):
        return self.length > 0

    def first(self):
        return self.elems[0] if not self.empty() else None

    def last(self):
        return self.elems[-1] if not self.empty() else None

    def push(self, elem):
        self.elems.append(elem)
        self.length += 1

    def pop(self):
        self.elems.pop()
        self.length -= 1

    def collect(self, predicate):
        new_types = []
        for elem in self.elems:
            if predicate(elem):
                new_types.append(elem)
        return IRList(new_types)

    def take(self, amount):
        return IRList(self.elems[0:amount])

    def filter(self, predicate):
        new = []
        for elem in self.elems:
            if predicate(elem):
                continue
            new.append(elem)
        return IRList(new)

    def iterate(self, callback):
        for elem in self.elems:
            callback(elem)

    def find(self, predicate):
        for elem in self.elems:
            if predicate(elem):
                return elem
        return None

    def transform(self, predicate):
        new = []
        for elem in self.elems:
            new.append(predicate(elem))
        return IRList(new)

    def as_enum(self):
        new = []
        for idx, elem in enumerate(self.elems):
            new.append((idx, elem))
        return IRList(new)

    def get(self, index):
        if index >= self.length:
            return None
        return self.elems[index]

    def insert(self, index, elem):
        if index >= self.length:
            return False
        self.elems.insert(index, elem)
        return True

    def remove(self, index):
        if index >= self.length:
            return None
        return self.elems.remove(index)

    def set(self, index, elem):
        if index >= self.length:
            return False
        self.elems[index] = elem
        return True

    def transform_if(self, elem, predicate):
        for idx, elem in enumerate(self.elems):
            if predicate(elem):
                self.elems[idx] = elem

    def zip(self, other):
        new = []
        index = 0
        while index < self.length:
            e1 = self.get(index)
            e2 = other.get(index)
            new.append((e1, e2))
        return IRList(new)

    def get_compares(self, comparitor):
        new = []
        for elem in self.elems:
            result = comparitor(elem)
            new.append((result, elem))
        return IRList(new)

    def join(self, default, joiner):
        joined = default
        for elem in self.elems:
            result = joiner(joined, elem)
            joined = result
        return joined

