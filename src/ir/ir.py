from .ir_list import IRList

class IRModule:
    def __init__(self, mod_name, path):
        ##The name of the module. This is usually just the file name
        self.mod_name = mod_name
        ##The path to the module. 
        ##This is the absolute path constructed at the start of compilation
        self.path = path
        ##This is a list of routines that are compiled from higher level functions
        ##List[IRRoutine]
        self.routines = []
        ##This is a list of defined types in the program
        ##List[IRTypeDef]
        self.types = IRList([])
        ##TODO: This is a list of defined exported globals.
        #self.globals = []

    ##TODO: We need a serializer here
    
    ##TODO: We need a deserializer here
    def dump(self):
        dump = self.types.join("", lambda last, current: last + f"{current.dump()}\n") 
        dump += "\n"
        for routine in self.routines:
            dump += routine.dump()
        return dump

class IRInstruction:
    def __init__(self, name, opcode, operands, loc):
        self.name = name
        self.opcode = opcode
        self.operands = operands
        self.loc = loc

    def dump(self):
        dump = f"{self.name}"
        for operand in self.operands:
            dump += f" {operand.dump()}"
        return dump

##This will allocate space on the stack of a given size (through the [typ] parameter)
##
##One big misconception about stack allocation is that usually the frame preallocates
##  enough space for all variables in the frame when the program enter's a new stack frame (routine call)
##So using this alloc really tells the compiler to acquire an address on the stack and store it somewhere
##  usually into a variable. Any time this is used, a pointer is acquired.
##
##Example:
##  Silver:
##      num = 5
##
##  IR:
##      %num *%u32 = alloc %u32
##      set *%u32, %u32 5
##
##This instruction usually comes in hand with set/get instructions.
class IRAlloc(IRInstruction):
    def __init__(self, typ, loc):
        super().__init__("alloc", 0x10, [typ], loc)
        self.type = typ

    def dump(self):
        return f"alloc {self.type.dump()}"

##This is an identifier mapped to a valued instruction.
##Some instructions do not produce a value. Arithmetics, set, and call do not produce values
##However, alloc, get, variable references, and immediate values (integer literals, floating-point literals, string literals, etc) are valued.
##So a variable can be assigned to any of the above mentioned instructions
##
##Example:
##  Silver:
##      num = 5
##
## IR:
##      %num *%u32 = alloc %u32
##      set %num *%u32, 5 %u32
class IRVariable(IRInstruction):
    def __init__(self, name, typ, alloc, loc):
        super().__init__("var", 0x20, [name, typ, alloc], loc)
        self.var_name = name
        self.typ = typ
        self.alloc = alloc

    def dump(self):
        return f"%{self.var_name} {self.typ.dump()} = {self.alloc.dump()}"

##A routine is a slightly higher level version of an assembly routine. All routines have an input and an output
##
##A routine will always have a pointer to a tuple representing the input arguments, 
##  and a pointer to a tuple representing the output arguments
##A function is always lowered such that its input and output are represented as stack allocated
##  tuples and then their pointers stored in the registers rdi (destination, aka output) and rsi (source, aka input)
##This makes it easier to compile any kind of function to its most basic form
##
##Example:
##  Silver source code:
##      def calc(a: u32, b: u32) -> u32:
##          return a * b
##
##  IR code:
##      type %u8 = opaque
##      type %u16 = %u8, %u8
##      type %u32 = %u16, %u16
##      type %u64 = %u32, %u32
##
##      type %tuple.0 = %u32, %u32
##      type %tuple.1 = u32
##
##      define %calc(%__in_args__ *%tuple.0, %__out_args__ *%tuple.1){
##          %0 *%u32 = get %__in_args__, 0
##          %1 *%u32 = get %__in_args__, 1
##          %3 *%u32 = alloc %u32
##          mul %0, %1, %3
##          set %__out_args__, 0, %3
##          ret
##      }
class IRRoutine(IRInstruction):
    def __init__(self, proc_name, in_type, out_type, body, loc):
        super().__init__("proc", 0x30, [proc_name, in_type, out_type, body], loc)
        self.proc_name = proc_name
        self.in_type = in_type
        self.out_type = out_type
        self.body = body

    def dump(self):
        dump = f"proc %{self.proc_name}(%__in_args__ {self.in_type.dump()}, %__out_args__ {self.out_type.dump()}){{\n"
        for ins in self.body:
            dump += f"    {ins.dump()}\n"

        dump += "}\n"

        return dump

##This is a type definition which uses the ir keyword `type`.
##A type is really just a list of other types.
##Types do not really have fields. Fields and other memebers are lowered to
##offsets in memory from the start of the type's allocation in memory
##
##A type definition is really just a memory layout that can be reused
##
##Example:
##  Silver source code:
##      struct Person(name: str, age: u32)
##      people: [5]Person = empty
##
##  IR code:
##      type %u8            = opaque
##      type %u16           = %u8, %u8
##      type %u32           = %u16, %u16
##      type %str           = *%u8
##      type %struct.Person = %str, %u32
##      type %array.0       = 5 %struct.Person ;This means there are 5 %struct.Person elements in this type
class IRTypeDef(IRInstruction):
    ##type_name = str
    ##type_list = IRList[Union[IRTypeRef, Tuple[int, IRTypeRef]]]
    def __init__(self, type_name, type_list, loc):
        assert isinstance(type_list, IRList)
        super().__init__("typedef", 0x30, [type_name, type_list], loc)
        self.type_name = type_name
        self.type_list = type_list

    def dump(self):
        assert isinstance(self.type_list, IRList)
        dump = f"type %{self.type_name} = "
        if self.type_list.empty():
            dump += "opaque"
        else:
            dump = self.type_list.transform(lambda ty: ty.dump()) \
                .as_enum() \
                .transform(lambda elem: elem[1] + ", " if elem[0] < self.type_list.length - 1 else elem[1]) \
                .join(dump, lambda last, current: last + current)

        return dump

class IRArrayType(IRInstruction):
    def __init__(self, size, inner_type, loc):
        super().__init__("arraytype", 0x32, [size, inner_type], loc)
        self.size = size
        self.inner_type = inner_type

    def dump(self):
        return f"{self.size} {self.inner_type.dump()}"

##This is a type reference, also called a `usage`. This is when a type definition is used to declare the type of something
##  such as a variable or a function return type
##For an example, see IRTypeDef for how a type definition is used in IR
class IRTypeRef(IRInstruction):
    def __init__(self, ty_def, loc):
        super().__init__("typeref", 0x41, [ty_def], loc)
        self.ty_def = ty_def

    def dump(self):
        return f"%{self.ty_def.type_name}"

class IRPointerType(IRInstruction):
    def __init__(self, type_ref, loc):
        super().__init__("ptr", 0x42, [type_ref], loc)
        self.ty_def = type_ref.ty_def
        self.type_ref = type_ref

    def dump(self):
        return f"*{self.type_ref.dump()}"

##This instruction moves the value in the given pointer into a given location, usually a variable.
##Since this gets a value from a pointer, and pointers may be pointing to an aggregate structure
##  in memory, such as an array or a tuple, then this takes an index to get a value in the pointer from.
##
##
##Example:
##  Silver:
##      num = 5
##      num
##
##  IR:
##      %num *%u32 = alloc %u32
##      set %num *%u32, %u32 5
##      %0 %u32 = get %num %*u32, 0 %u32
##
##With arrays:
##  Silver:
##      nums = [1, 2, 3]
##      num = nums[1]
##
##  IR:
##      type %array.0 = 3 %u32
##
##      %nums %array.0 = alloc %array.0
##      set %nums %array.0, 0 %u32, [ 1 %u32, 2 %u32, 3 %u32 ]
##      %num *%u32 = alloc %u32
##      %0 %u32 = get %nums %array.0, 1 %u32
##      set %num *%u32, 0 %u3, %0
class IRGet(IRInstruction):
    def __init__(self, subject, index, loc):
        super().__init__("get", 0x40, [subject, index], loc)
        self.subject = subject
        self.index = index
        
    def dump(self):
        return f"get {self.subject.dump()}, {self.index.dump()}"

##This instruction moves a given value into the address of a given pointer, at a given index.
##Variables will often use this instruction along with IRAlloc to initialize on the stack using the 0th index
## however, arrays and tuples may initialize with an index, or may transform at an index
##
##Example:
##  Silver:
##      num = 5
##
##  IR:
##      %num *%u32 = alloc %u32
##      set %num *%u32, %u32 5
##For arrays, see the IRGet comment above
class IRSet(IRInstruction):
    ##subject = IRVariableRef
    ##obj = IRExpression
    ##index = IRConstant
    def __init__(self, subject, obj, index, loc):
        super().__init__("set", 0x41, [subject, obj, index], loc)
        self.subject = subject
        self.obj = obj
        self.index = index

    def dump(self):
        return f"set {self.subject.dump()}, {self.obj.dump()}, {self.index.dump()}"

class IRBinaryArithmetic(IRInstruction):
    def __init__(self, name, opcode, left, right, out, loc):
        super().__init__(name, opcode, [left, right, out], loc)
        self.left = left
        self.right = right
        self.out = out

    def dump(self):
        return f"{self.name} {self.left.dump()}, {self.right.dump()}, {self.out.dump()}"

class IRAdd(IRBinaryArithmetic):
    def __init__(self, left, right, out, loc):
        super().__init__("add", 0x50, left, right, out, loc)
    

class IRSub(IRBinaryArithmetic):
    def __init__(self, left, right, out, loc):
        super().__init__("sub", 0x50, left, right, out, loc)


class IRMul(IRBinaryArithmetic):
    def __init__(self, left, right, out, loc):
        super().__init__("mul", 0x50, left, right, out, loc)


class IRDiv(IRBinaryArithmetic):
    def __init__(self, left, right, out, loc):
        super().__init__("div", 0x50, left, right, out, loc)


##This is just a simple variable reference, different from a memory reference
##
##Example:
##  Silver:
##      def calc(x: u32, y: u32):
##          return x * y
##
##      result = calc(5, 3)
##      num = result / 2
##            ------
##
##  IR:
##      %0 *%tuple.0 = alloc %tuple.0
##      set %0, 0 %u32, 5
##      set %0, 1 %u32, 3
##      %result *%tuple.1 = alloc %tuple.1
##      call %calc, %0, %1
##      %num *%u32 = alloc %u32
##      %2 *%u32 = alloc %u32
##      set %2, 0 %u32, 2 %u32
##      %3 %u32 = get %result, 0 %u32
##      %4 %u32 = get %2 %u32, 0 %u32
##      mul %4, %3, %num
class IRVariableRef(IRInstruction):
    def __init__(self, var_name, ref_type, loc):
        super().__init__("ref", 0x11, [var_name, ref_type], loc)
        self.var_name = var_name
        self.typ = ref_type

    def dump(self):
        return f"%{self.var_name} {self.typ.dump()}"


##This is an array literal which is an allocated space of same sized values on the stack.
##
##Example:
##  Silver:
##      nums = [1, 2, 3]
##
##  IR:
##      type %u8 = opaque
##      type %u16 = %u8, %u8
##      type %u32 = %u16, %u16
##      type %array.0 = 3 %u32
##
##      %nums *%array.0 = alloc %array.0
##      set %nums *%array.0, 0 %u32, %array.0 [ 1 %u32, 2 %u32, 3 %u32 ]
class IRArrayLiteral(IRInstruction):
    def __init__(self, arr_type, elements, loc):
        super().__init__("arr", 0x50, [arr_type, elements], loc)
        self.arr_type = arr_type
        self.elements = elements

    def dump(self):
        dump = f"{self.arr_type.dump()} ["
        for idx, elem in enumerate(self.elements):
            dump += elem.dump()
            if idx < arr_type.size - 1:
                dump += ", "
        dump += "]"
        return dump

##This is just a fixed, multi-typed array of values on the stack. All structs are lowered to tuples.
##It can also be considered just a list of contiguous values in memory.
##
##Example:
##  Silver:
##      nums = ("alex", 24)
##
##  IR:
##      type %u8            = opaque
##      type %u16           = %u8, %u8
##      type %u32           = %u16, %u16
##      type %str.0         = *%u32
##      type %tuple.0       = 3 %u32
##
##      %nums *%array.0 = alloc %array.0
##      set %nums *%array.0, 0 %u32, %array.0 [ 1 %u32, 2 %u32, 3 %u32 ]
class IRTupleLiteral(IRInstruction):
    def __init__(self, tup_type, elements, loc):
        super().__init__("tuple", 0x51, [tup_type, elements], loc)
        self.tup_type = tup_type
        self.elements = elements

    def dump(self):
        dump = f"{self.tup_type.dump()} ("
        for idx, elem in enumerate(self.elements):
            dump += elem.dump()
            if idx < self.tup_type.size - 1:
                dump += ", "
        dump += ")"
        return dump


##This returns from the current routine to the calling routine by doing the following:
##  Step 1. Moving any return values into the routine's out_ptr
##  Step 2. Shrinking the stack by using `add rsp, [stack_size]`
##              where [stack_size] is the allocated stack size for the current frame
##  Step 3. Return back using `ret` or something equivalent (if compiling for a non-x86-based architecture)
class IRReturn(IRInstruction):
    def __init__(self, loc):
        super().__init__("ret", 0x32, [], loc)

    def dump(self):
        return "ret"

##Jumps to the location of the given routine by first doing the following:
##  1. Allocating space on the stack for the out_ptr, which is the place on the stack where
##      the return value will go. For routines that don't return anything, this will 
##      simply point to a one byte allocated address on the stack, but it will never be used
##  2. Allocating space on the stack for the in_ptr, which is the place on the stack where
##      the input arguments will go.
##  3. Creating a new stack frame. In assembly code, this will be
##          push rbp
##          mov  rbp, rsp
##          sub  rsp, [frame_size]
##      where [frame_size] is the size of the frame. This is necessary because
##      sometimes, we have to use relative offsetting with address displacement, which using
##      rbp to offset for a variable doesn't work in the positive displacement, 
##      so using rsp for displacement works better. This is good for array indexing, etc
##
class IRCall(IRInstruction):
    def __init__(self, routine_ident, in_ptr, out_ptr, loc):
        super().__init__("call", 0x31, [routine_ident, in_ptr, out_ptr], loc)
        self.routine_ident = routine_ident
        self.in_ptr = in_ptr
        self.out_ptr = out_ptr

    def dump(self):
        return f"call %{self.routine_ident} {self.in_ptr.dump()}, {self.out_ptr.dump()}"

##This is a constant. Because of python's lack of a type system, [value] can be 
##  of any type and will just be converted to a string by python. Thanks python lmao
##
##Example:
##  Silver:
##      name = "alex"
##      age = 24
##
##  IR:
##      type %u8    = opaque
##      type %u16   = %u8, %u8
##      type %u32   = %u16, %u16
##      type %u64   = %32, %32
##      type %str   = *%u8
##      type %str.0 = 5 %u8
##      
##      const %const.str.0 %str.0 = "alex"
##
##      TODO: Fix this example with real compiler generated example
##      %name %str.0 = ref %str.0 %const.str.0, 0 %u32 ;See IRRef
##      %age *%u32 = alloc %u32
##      set %age *%u32, 24 %u32
##
class IRConstant(IRInstruction):
    def __init__(self, const_type, value, loc):
        super().__init__("constant", 0x70, [const_type, value], loc)
        self.const_type = const_type
        self.value = value

    def dump(self):
        return f"{self.value} {self.const_type.dump()}"

   
