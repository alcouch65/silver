import sys
import os
from os import path
import subprocess
import shutil
from collections import deque

from parser import Parser
from tokenizer import Tokenizer
#from typecheck import TypeChecker
#from codegen import Codegen
from ir.lowering import ASTLowering
from iota import iota
from type_builder import TypeBuilder
from sema import Sema

class Compiler:
    def __init__(self, src_path=None):
        self.src_path = src_path
        with open(src_path, 'r') as f:
            self.src_str = f.read()
            self.tokenizer = Tokenizer(self)

            self.type_builder = TypeBuilder(self)
            self.parser = Parser(self, self.type_builder)
            #self.tychecker = TypeChecker(self, type_builder)
            #self.codegen = Codegen(self)
            self.options = {
                "dump_location": "",
                "dump_tokens": False,
                "dump_parsed": False, 
                "dump_typed": False,
                "out_dir": os.getcwd() + "/out",
                "keep_asm": False,
                "keep_obj": False,
                "out_name": ""
            }

    def get_options(self, args):
        arg_index = 0
        while arg_index < len(args):
            arg = args[arg_index]
            if not arg.startswith('-'):
                print(f"Internal: Unexpected arg {arg}")
                arg_index += 1
                continue
            option_name = arg[1:]
            if option_name not in self.options:
                print(f"Internal: Unrecognized compiler option {option_name}. Use -help for a list of options")
                arg_index += 1
                continue
            if option_name == "out_dir" or option_name == "out_name" or option_name == "dump_location":
                arg_index += 1
                out_dir = args[arg_index]
                self.options[option_name] = out_dir
            elif option_name == "help":
                print("silver [options] ... file.ag")
                print("Compiler Options:")
                print("    -help : Display this help text")
                print("    -dump_location : The location to dump debug info to, such as tokens or bytecode. Not setting this will set it to stdout")
                print("    -dump_tokens : Dump the tokens to dump_location")
                print("    -dump_parsed : Dump the intermediate representation after parsing and before semantic analysis")
                print("    -dump_typed : Dump the intermediate representation after type checking to dump_location")
                print("    -out_dir : The directory to write output files such as the executable or asm/.o files")
                print("    -keep_asm : Keep the generated assembly file in the out_dir")
                print("    -keep_obj : Keep the generated object file in the out_dir")
                print("    -out_name : Set the name of the output files (out.asm, out.o, out)")
            else:
                self.options[option_name] = True
            arg_index += 1

    def dump_type(self, ty):
        ty_str = ""
        if ty is None:
            return "None"
        if ty['kind'] == "primitive":
            if ty['ident'] == "ref":
                ty_str = f"ref {self.dump_type(ty['data'])}"
            else:
                ty_str += ty['ident']
        elif ty['kind'] == "aggregate":
            if ty['ident'] == "array":
                arr_data = ty['data']
                arr_type = arr_data['type']
                ty_str += "[" + str(ty['data']['size']) + ']'
                if arr_type['ident'] == "primitive":
                    ty_str += arr_type['ident']
                else:
                    ty_str += self.dump_type(arr_type)
            
        else:
            ty_str = ty['ident']
        return ty_str

    def dump_opcode(self, bc):
        bc_str = ""
        # print(f"bc = {bc}")
        bc_str += f"{BYTECODE_LOOKUP_TABLE[bc['opcode']]}"
        if len(bc_str) < 16:
            space_count = 16 - len(bc_str)
            for i in range(space_count):
                bc_str += ' '
        for (index, operand) in enumerate(bc['operands']):
            if isinstance(operand, dict):
                if 'kind' in operand:
                    bc_str += self.dump_type(operand)
            else:
                bc_str += str(operand)
            if index + 1 < len(bc['operands']):
                bc_str += ', '
        return bc_str

    def dump_bytecode(self, bytecode):
        dump_str = ""
        while bytecode.has_next():
            bc = bytecode.next_opcode()
            bc_str = self.dump_opcode(bc)
            dump_str += bc_str + '\n'
        bytecode.reset()
        return dump_str

    def dump_tokens(self, tokens):
        dump_str = ""
        for token in tokens:
            tk_str = str(token)
            dump_str += tk_str + '\n'
        return dump_str

    def compile(self):
        tokens = self.tokenizer.lex_file()
        if tokens is None:
            print("Tokens = None")
            return None
        if self.options['dump_tokens']:
            if self.options['dump_location'] != "":
                with open(self.options['dump_location'], 'w') as dump_file:
                    dump_file.write(self.dump_tokens(tokens))
            else:
                print(self.dump_tokens(tokens))
        ast = self.parser.parse(tokens)
        if ast is None:
            return None
        if self.options['dump_parsed']:
            if self.options['dump_location'] != "":
                with open(self.options['dump_location'], 'w') as dump_file:
                    dump_file.write(ast.dump())
            else:
                print(ast.dump())
        sema = Sema(self, ast)
        if not sema.resolveSymbols():
            print("Failed to resolve symbols")
            return None
        if not sema.collectAndCheckTypes():
            print("Type checking failed!")
            return None
        if self.options['dump_typed']:
            if self.options['dump_location'] != "":
                with open(self.options['dump_location'], 'w') as dump_file:
                    dump_file.write(ast.dump())
            else:
                print(ast.dump())
        lowering = ASTLowering(self, ast, sema.symbol_table, self.type_builder)
        module = lowering.lower_ast()
        if not module:
            print("Failed to lower ast")
            return None
        print(module.dump())
        return None 


argv = sys.argv
(program, *args) = argv[0:]
src_path = args[-1] #Check for other command-line options such as debug flags and stuff
options = args[:-1]
compiler = Compiler(src_path)
compiler.get_options(options)
code = compiler.compile()

if code is None:
    exit(1)

cwd = os.getcwd()
out_dir = compiler.options['out_dir']
if os.path.exists(out_dir) and os.path.isdir(out_dir):
    shutil.rmtree(out_dir)
os.mkdir(out_dir)
path, ext = path.splitext(src_path)
##      ASM Writing     ##
path_dir, path_file = os.path.split(path)
out_path = out_dir + '/' + path_dir
asm_out =  out_path + '/' + path_file + '.asm'
print("Generating assembly code to %s" % asm_out)
if not os.path.exists(out_path):
    os.mkdir(out_path)
asm_file = open(asm_out, 'w')
asm_file.write(code)
asm_file.close()
##      Compiling/Assembling    ##
bin_out = out_path + "/" + path_file
obj_out = bin_out + '.o'
ret = subprocess.call(["nasm", "-felf64", asm_out])
if ret:
    print("Failed to generate object file")
    exit(ret)
print(f"Generating object file to {obj_out}")
ret = subprocess.call(["ld", "-o", bin_out, obj_out])
if ret:
    print("Failed to generate executable file")
    exit(ret)
print(f"Generating executable file to {bin_out}")

if not compiler.options['keep_asm']:
    os.remove(asm_out)
if not compiler.options['keep_obj']:
    os.remove(obj_out)
