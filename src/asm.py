from error import *#noqa
from iota import *#noqa

class ASMModule:
    def __init__(self, name=None):
        self.name = name
        self.routines = []

    def dump(self):
        dump = ""
        dump += "BITS 64\n"
        dump += "global _start\n"
        dump += "section .text\n"
        for routine in self.routines:
            routine_dump = routine.dump()
            routine_lines = routine_dump.splitlines()
            for routine_line in routine_lines:
                dump += f"{routine_line}\n"
        return dump


class ASMElement:
    def __init__(self, name = None):
        self.name = name

    def dump(self):
        pass

class ASMRoutine(ASMElement):
    def __init__(self, name=None):
        super().__init__(name)
        self.instructions = []

    def dump(self):
        dump = ""
        dump += f"{self.name}:\n"
        for ins in self.instructions:
            dump += f"    {ins.dump()}\n"
        return dump
class Label:
    def __init__(self, label_ident=None, label_loc=None):
        self.label_ident = label_ident
        self.label_loc = label_loc
##ASMStatement represents the paring of an opcode with operands
##An asm statement does not necessarily represent any kind of existing ASM
##  it merely seeks to be a mock representation of all kinds of ASM
##  which can then be used to generate proper ASM such as x86-64, ARM64, WASM, etc
class ASMStatement(ASMElement):
    def __init__(self, name = None, opcode=None, operands=[]):
        super().__init__(name)
        self.opcode = opcode
        self.operands = operands
        self.operands_size = len(operands)

    def transform_operand(self, index, value):
        assert index < len(self.operands), f"Cannot transform operand with index {index} greater than size of operands {len(self.operands)}"
        self.operands[index] = value

    def add_operand(self, value):
        self.operands.append(value)
        self.operands_size += 1

    def remove_operand(self, index):
        self.operands.remove(index)
        self.operands_size -= 1
    
class ASMPush(ASMStatement):
    def __init__(self, value=None):
        super().__init__("push", 6, [value])
        self.value = value

    def dump(self):
        return f"push   {self.value.dump()}"

class ASMMov(ASMStatement):
    ##stack_offset = ASMAddress
    ##value = ASMValue
    ##
    ##TODO: Add optional size because sometimes we need to specify 
    ##      how much data to occupy on the stack

    def __init__(self, dest, source, size):
        assert dest is not None, "ASMMov:dest cannot be None"
        assert source is not None, "ASMMov:source cannot be None"
        super().__init__("mov", 1, [dest, source])
        self.size = size
        self.dest = dest
        self.source = source

    def dump(self):
        return f"{self.name}    {self.size} {self.dest.dump()}, {self.size} {self.source.dump()}"

 
class ASMCall(ASMStatement):
    ##address = ASMValue
    def __init__(self, address=None):
        super().__init__("call", 3, [address])
        self.address = address

    def dump(self):
        return f"call    {self.address}"

class ASMBinary(ASMStatement):
    def __init__(self, name=None, lhs=None, rhs=None):
        super().__init__(name, 5, [lhs, rhs])
        self.lhs = lhs
        self.rhs = rhs

    def dump(self):
        return f"{self.name}    {self.lhs.dump()}, {self.rhs.dump()}"

class ASMMul(ASMBinary):
    def __init__(self, source=None):
        assert source is not None, "ASMMul:source cannot be None"
        super().__init__("mul", 10, [source])
        self.source = source

    def dump(self):
        return f"mul    {self.source.dump()}"

class ASMReturn(ASMStatement):
    def __init__(self):
        super().__init__("ret", 4, [])

    def dump(self):
        return "ret"

class ASMValue(ASMElement):
   def __init__(self, name, kind, value, size=None):
       super().__init__(name)
       self.kind = kind
       self.size = size
       self.value = value

class ASMRegister(ASMValue):
    def __init__(self, value):
        assert value is not None, "value of ASMRegister cannot be None"
        super().__init__("reg", "reg", value)
    
    def dump(self):
        return f"{self.value}"

class ASMImmediate(ASMValue):
    def __init__(self, value=None, size=None):
        assert isinstance(value, int), f"ASMImmediate:value must be int but is {type(value)}"
        super().__init__("imm", "imm", value, size)

    def dump(self):
        return f"{self.value}"

##A pointer to some address either of the stack or heap or within the program
## that has been displaced
##
##Pointer offsetting occurs when a program needs to get the value at an address
##  relative to another address
##
##Arrays are a good example of needing pointer offsetting
##  To access an array at an index, you will need to offset relative to the size of each elemenet
##
##code:
##  nums: [10]u32 = empty
##  nums[2] = 5
##
##Here, we are creating a zero-init'd array of 10 u32 elements. So each element is 4 bytes.
##  At index 2, we are writing the integer 5. Since ararys are indexed starting at 0,
##  we have to do
##      ... ;; 0-init array here
##      mov     eax, 2          ;; Index
##      mov     [rbp-(eax*4)], 5    ;; Value at array index
##  However, this is not realistic for a compiler. We want to actually calculate the offset
##  ourselves so it actually ends up looking like this:
##      ...     ;; 0-init array here
##      lea     eax, [rbp-0]    ;; We want to load the effective address of the array, 
##                              ;; in this case is the start of the current stack frame, rbp - 0
##      mov     [eax - 8], 5    ;; We want to use the effective address loaded into eax register 
##                              ;; to write to the array with an offset of 8
##  Since our index is 2, and the base pointer is loaded into eax, we have to assume that
##  the base pointer is index 0. The element index is going to be element_size*index, therefore
##  our index is base+(index*scale)+displacement. However, we are not using a displacement right now
##  Our base is loaded into eax
##  Our index is 2
##  Our scale is the size of u32
##  But during compilation, when we implement structs, accessing a member of a struct within an array
##  will require some kind of displacement based on the location of the member
##
##      struct Person(name: str, age: u32)
##      ...
##      people: [10]Person = empty
##      people[2] = Person("name", 24)
##
##      ;; Insert example asm code here
class ASMPointer(ASMValue):
    ##address = ASMRegister
    ##offset = ASMImmediate
    def __init__(self, address=None, base=None, scale=None, displacement=None):
        assert address is not None, "address cannot be None"
        if base is not None:
            assert not isinstance(base, str)
        super().__init__("ptr", "ptr", address)
        self.address = address
        self.base = base
        self.scale = scale
        self.displacement = displacement

    def dump(self):
        dump = f"[{self.address.dump()}"
        if self.base:
            if isinstance(self.base, ASMImmediate):
                if self.base.value < 0:
                    dump += " - "
                    dump += f"{-self.base.value}"
                elif self.base.value > 0:
                    dump += " + "
                    dump += f"{self.base.value}"
            else:
                print(self.base)
                dump += f" + {self.base.dump()}"
            if self.scale:
                dump += f" * {self.scale.dump()}"
            if self.displacement:
                if isinstance(self.displacement, ASMImmediate):
                    if self.displacement.value < 0:
                        dump += " - "
                        dump += f"{-self.displacement.value}"
                    elif self.displacement.value > 0:
                        dump += " + "
                        dump += f"{self.displacement.value}"
                else:
                    dump += f" + {self.displacement.dump()}"
        dump += "]"
        return dump

 
class ASMPop(ASMStatement):
    def __init__(self, dest):
        super().__init__("pop", 7, [dest])
        self.dest = dest

    def dump(self):
        return f"pop    {self.dest.dump()}"

class ASMSyscall(ASMStatement):
    def __init__(self):
        super().__init__("syscall", 8, [])

    def dump(self):
        return "syscall"

class ASMLEA(ASMStatement):
    ##register = ASMRegister
    ##address = ASMPointer
    def __init__(self, register, address):
        super().__init__("lea", 9, [register, address])
        self.register = register
        self.address = address

    def dump(self):
        return f"lea    {self.register.dump()}, {self.address.dump()}"
