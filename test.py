import sys
import os
from os import path
import subprocess

cwd = os.getcwd()
tests_dir = "samples"
for file in os.listdir(tests_dir):
    path = tests_dir + '/' + file
    print(f"Testing {path}...")
    try:
        ret = subprocess.call(["python3", "src/silver.py", "-dump_typed", "-dump_parsed", path])
        print("Success!")
    except subprocess.CalledProcessError as e:
        print("Failed!")
