# Silver Programming Language
Silver is a python like language that compiles to machine code aimed to clean up the oddities of python. This is just a toy language. I start this because I hadn't been programming for most of 2021 due to being sick from a severe refractory Crohn's flare and needing ostomy surgery. After I had recovered enough from surgery I felt better enough to get back into programming and this is what I conjured up.

## Current Progress
The following [TODO](#todo) list shows everything that has been completed and will be done before self-hosting is started

## TODO
- Pipeline
    - IR
        - So far I have redesigned the IR to be object based instead of in the form of bytecode
        - I will need to do semantic analysis in order to transform types and do desuguring
        - Symbols and symbol resolution has been added
        - So far only types have been transformed in order to add types to statements and expressions
- Parsing
    - Statements
        - [x] Variables
        - [x] Functions
        - [ ] Structs
        - [ ] Imports
    - Expressions
        - [x] Integers
            - [x] 8-bit
            - [x] 16-bit
            - [x] 32-bit
            - [x] 64-bit
            - [ ] Signed
        - [ ] Floats
        - [ ] Strings
        - [x] Arrays
            - [x] Array Literals
            - [x] Array Indexing
            - [x] Array Set/Get
        - [x] Chars
        - [ ] Tuples
        - References
            - [x] Var Copy
                - Example
                ```
                num = 5 + 3
                copy = num #Copy
                ```
            - [x] Var Ref
                - Example
                ```
                num = 5 + 3
                var_ref = ref num #Reference
                ```
        - Math
            - [x] Add
            - [x] Sub
            - [x] Mul
            - [x] Div
        - [x] Function calls
    - [ ] Inline Assembly
    
- Type Checking
    - Done
        For now...

- Semantic Analysis
    - Step 1:
        Symbol Resolution
    - Step 2:
        Type Checking
    - Step 3:
        Desugaring
        - Step 1:
            - Polymorphic Resolution
                - transform generic structs and methods into concrete types
        - Step 2:
            - Struct Desugaring
                - Convert Struct methods into ordinary functions that take the first parameter `self` as an instance of Self
        - Step 3:
            - Convert sugary loops into while loops
                - For-each => for range => while
        - Step 4:
            - Convert pattern matching to if-elif complex

Code Generation
    - Simulate runtime with registers and a stack
        - Lower into instruction-like lower IR to simulate operations at runtime
    - Convert lower IR to ASM code
